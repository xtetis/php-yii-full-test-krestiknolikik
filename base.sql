-- phpMyAdmin SQL Dump
-- version 4.0.5
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июн 28 2015 г., 12:42
-- Версия сервера: 5.5.39-MariaDB
-- Версия PHP: 5.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `testxtetis_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `xta_game`
--

DROP TABLE IF EXISTS `xta_game`;
CREATE TABLE IF NOT EXISTS `xta_game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user_creator` int(11) NOT NULL,
  `id_user_gamer` int(11) NOT NULL,
  `gamer_accept` int(11) NOT NULL DEFAULT '0',
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `aborted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Список игр' AUTO_INCREMENT=119 ;

--
-- Дамп данных таблицы `xta_game`
--

INSERT INTO `xta_game` (`id`, `id_user_creator`, `id_user_gamer`, `gamer_accept`, `createdon`, `aborted`) VALUES
(116, 13, 12, 1, '2015-06-28 09:31:39', 1),
(117, 13, 12, 1, '2015-06-28 09:33:37', 1),
(118, 12, 13, 1, '2015-06-28 09:34:28', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `xta_step`
--

DROP TABLE IF EXISTS `xta_step`;
CREATE TABLE IF NOT EXISTS `xta_step` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_game` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_game` (`id_game`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Список ходов' AUTO_INCREMENT=416 ;

--
-- Дамп данных таблицы `xta_step`
--

INSERT INTO `xta_step` (`id`, `id_game`, `id_user`, `position`) VALUES
(393, 116, 12, 5),
(394, 116, 13, 3),
(395, 116, 12, 1),
(396, 116, 13, 9),
(397, 116, 12, 6),
(398, 116, 13, 4),
(399, 116, 12, 8),
(400, 116, 13, 2),
(401, 116, 12, 7),
(402, 117, 12, 5),
(403, 117, 13, 3),
(404, 117, 12, 2),
(405, 117, 13, 8),
(406, 117, 12, 4),
(407, 117, 13, 6),
(408, 117, 12, 9),
(409, 117, 13, 1),
(410, 117, 12, 7),
(411, 118, 13, 5),
(412, 118, 12, 3),
(413, 118, 13, 2),
(414, 118, 12, 6),
(415, 118, 13, 8);

-- --------------------------------------------------------

--
-- Структура таблицы `xta_user`
--

DROP TABLE IF EXISTS `xta_user`;
CREATE TABLE IF NOT EXISTS `xta_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) NOT NULL DEFAULT '' COMMENT 'Email',
  `pass` varchar(200) NOT NULL DEFAULT '' COMMENT 'Пароль',
  `username` varchar(200) NOT NULL DEFAULT '' COMMENT ' Контактное лицо ',
  `hash` varchar(200) NOT NULL COMMENT 'Хеш, который устанавлявается при авторизации',
  `lastlogin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Последняя авторизация',
  `lastactivity` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `name` (`email`,`pass`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Список пользователей' AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `xta_user`
--

INSERT INTO `xta_user` (`id`, `email`, `pass`, `username`, `hash`, `lastlogin`, `lastactivity`) VALUES
(12, 'tihonenkovaleriy@gmail.com', '43e3058d7918bb16fa1f3f01778560e2', 'Валерий', '4545601795b8975a33b8419f4f55730e', '2015-06-28 09:30:17', '2015-06-28 12:42:30'),
(13, 'qwecdf2gd222s2rr@bk.ru', 'b99937535a8405c947a37947cb775575', 'Вася', '2b00c5e8cde9555c1fb7e27bff25c334', '2015-06-28 09:31:14', '2015-06-28 12:35:06');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `xta_step`
--
ALTER TABLE `xta_step`
  ADD CONSTRAINT `xta_step_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `xta_user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `xta_step_ibfk_1` FOREIGN KEY (`id_game`) REFERENCES `xta_game` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
