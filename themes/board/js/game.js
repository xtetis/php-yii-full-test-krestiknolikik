//==============================================================================
function create_game(_gamer){
  var jqxhr = $.post( "/site/creategame", {gamer:_gamer} ,function(data) {
    var obj = jQuery.parseJSON(data);
    if (obj.status==0){
       $("#last_active_users").load("/site/getlastactiveasers");
       }
    if (obj.status==1){
       $("#last_active_users").load("/site/getlastactiveasers");
       }
    if (obj.status==2){
       //alert('status=2');
       // Начинаем игру
       }
  });
}
//==============================================================================










//==============================================================================
function abort_game(_game){
  var jqxhr = $.post( "/site/abortgame", {game:_game} ,function(data) {
         $("#last_active_users").load("/site/getlastactiveasers");
      });
}
//==============================================================================







//==============================================================================
function start_game(_game){
  if(window.askgameinterval !== null){
    clearInterval(window.askgameinterval);
    }
  var jqxhr = $.post( "/site/startgame", {game:_game} ,function(data) {
         $(".modal-body").html("");
         $('#game_modal').modal('show');
         $(".modal-body").load("/site/game/"+_game);
      });
}
//==============================================================================





//==============================================================================
function set_step(_game,_step){
  var jqxhr = $.post("/site/setstep", {game:_game,step:_step} ,function(data) {
         data =Number(data);
         //alert(data);
         if (data==0){ // Сделан корректный шаг
            $(".modal-body").load("/site/game/"+_game+"");
            }
         if (data==1){
            $('#gamealert').fadeOut('fast').fadeIn('fast').fadeOut('fast').fadeIn('fast');
            }
         if (data==2){
            $('#cell_'+_step).fadeOut('fast').fadeIn('fast').fadeOut('fast').fadeIn('fast');
            $('#step_alert_2').fadeIn('slow').fadeOut('slow');
            }
         if (data==3){
            $('#step_alert_3').fadeIn('slow').fadeOut('slow').fadeIn('slow').fadeOut('slow');
            setTimeout(function() {  
                                   $(".modal-body").load("/site/askgame/"+_game);
                                  }, 2000);
            }

      });
}
//==============================================================================







//==============================================================================
function check_go_step(game){
  var jqxhr = $.get( "/site/candostep/"+game ,function(data) {
        // alert(data);
         var current_inp_step_status = $('#inp_step_status').val();
         current_inp_step_status = Number(current_inp_step_status);
         if (data==1){
            if(current_inp_step_status==0){ // Если оппонент походил - требуется обновление
              //clearInterval(interval__check_go_step);
              stop_all_intervals();
              $(".modal-body").load("/site/game/"+game+"?notimer=1");
              }
            }
         if (data==3){ // Если ходы закончились
            stop_all_intervals();
            $('#step_alert_3').fadeIn('slow').fadeOut('slow').fadeIn('slow').fadeOut('slow');
            $("#tempdiv").load("/site/Closegame/"+game);
            setTimeout(function() {
                                //alert(game+' = 3');
                                $(".modal-body").load("/site/askgame/"+game);
                                }, 3000);
            }
            
         if (data==11){ // Если Вы победили
            stop_all_intervals();
            $('#step_alert_4').fadeIn('slow').fadeOut('slow').fadeIn('slow').fadeOut('slow');
            $("#tempdiv").load("/site/closegame/"+game);
            setTimeout(function() {  
                                //alert(game+' = 11');
                                $(".modal-body").load("/site/askgame/"+game);
                                }, 3000);
            }
            
         if (data==12){ // Если Вы проиграли
            stop_all_intervals();
            $(".modal-body").load("/site/game/"+game+"?notimer=1");
            setTimeout(function() { 
            $('#step_alert_5').fadeIn('slow').fadeOut('slow').fadeIn('slow').fadeOut('slow');
            $("#tempdiv").load("/site/closegame/"+game);
            setTimeout(function() { 
                                //alert(game+' = 12'); 
                                $(".modal-body").load("/site/askgame/"+game);
                                }, 3000);
                      }, 1000);          
            }
      });
}
//==============================================================================









//==============================================================================
function stop_all_intervals(){
  var interval_id = window.setInterval("", 9999); // Get a reference to the last
                                                  // interval +1
  for (var i = 1; i < interval_id; i++)
          window.clearInterval(i);
          
  for (var i = 1; i < 99999; i++)
          window.clearInterval(i);
  interval_refresh_users();
}
//==============================================================================




//==============================================================================
function interval_refresh_users(){
    setInterval(function(){$("#last_active_users").load("/site/getlastactiveasers");}, 5000);
    setInterval(function(){$("#hiddendiv").load("/site/activity");}, 5000);
}
//==============================================================================










$(function() {


  $(".modalclose").click(function() {
    if (confirm('Вы уверены что хотите закрыть окно?')){
       $('#game_modal').modal('hide');
       }
  });




/*
  $("#btn_create_game").click(function() {
    var _gamer = $(this).attr('idx');
    create_game(_gamer);
  });





$("#btn_abort_game").click(function() {
  var _game = $(this).attr('idx');
  abort_game(_game);
});
*/

});




