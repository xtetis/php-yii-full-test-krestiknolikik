$(function() {

$("#board_item_send_message").click(function() {
  
  $('#message_alert').hide();
  var sdata = $("#frm__send_message_to_user").serialize();

  var jqxhr = $.post( "/ajax/sendmessage", sdata ,function(data) {
    var obj = jQuery.parseJSON(data);
    if (obj.count_errors>0){
       $("#message_alert").removeClass('alert-info');
       $("#message_alert").addClass("alert-danger");
       $('#message_alert').html(obj.error);
       $('#message_alert').show();
    }else{
       $("#message_alert").removeClass('alert-danger');
       $("#message_alert").addClass("alert-info");
       $('#message_alert').html(obj.info_message);
       $('#inp__obj_send_message').val('');
       $('#inp__obj_send_email').val('');
       $('#inp__obj_send_name').val('');
       $('#message_alert').show();
    }
  });
  
  
});













$("#account_send_message").click(function() {
  
  var _message = $('#inp__obj_send_message').val();
  var _to = $('#inp__obj_to_user').val();


  var jqxhr = $.post( "/ajax/sendmessage", {message:_message,to:_to,command:'account_send'} ,function(data) {
    var obj = jQuery.parseJSON(data);
    if (obj.count_errors>0){
       $("#message_alert").removeClass('alert-info');
       $("#message_alert").addClass("alert-danger");
       $('#message_alert').html(obj.error);
       $('#message_alert').show();
    }else{
       $("#message_alert").removeClass('alert-danger');
       $("#message_alert").addClass("alert-info");
       $('#message_alert').html(obj.info_message);
       $('#inp__obj_send_message').val('');
       var d = new Date();
       var n = d.getTime();
       $('#frame_message_dlg_user').attr('src','/account/messagelist/'+_to+'?time='+n);
       $('#message_alert').show();
    }
  });
  
  
});








$("#btn_forgotpass").click(function() {
  var _email = $('#inp__forgotpass_email').val();
  var jqxhr = $.post( "/ajax/forgotpass", {email:_email} ,function(data) {
    var obj = jQuery.parseJSON(data);
    if (obj.count_errors>0){
       $("#message_alert").removeClass('alert-info');
       $("#message_alert").addClass("alert-danger");
       $('#message_alert').html(obj.error);
       $('#message_alert').show();
    }else{
       $("#message_alert").removeClass('alert-danger');
       $("#message_alert").addClass("alert-info");
       $('#message_alert').html(obj.info_message);
       $('#message_alert').show();
    }
  });
});













// При нажатии на 1 уровне выбора категории
//===============================================================================================
$(".rubr_item").click(function() {
  var idx=$(this).attr('idx');
  $('.rubr_item').removeClass('rubr_item_selected'); 
  $(this).addClass('rubr_item_selected'); 
  $('.rubr_item_second').hide(); 
  $('.parent_'+idx).show(); 
  $('.rubr_item_third').hide();
  $('.parent_'+idx).scrollTop();
});
//===============================================================================================





//===============================================================================================
$(".a__cat_a_select").click(function() {
  var idx=$(this).attr('idx');
  $('#select_categs_parents').hide(); 
  $('#select_categs_parents_lvl2').show(); 
  $('#select_categs_parents_lvl2_id'+idx).click(); 
  $('#rubric_1').scrollTop();
});
//===============================================================================================


//===============================================================================================
$(".rubr_item_lvl2").click(function() {
  var idx=$(this).attr('idx');
  $('.rubr_item_lvl2').removeClass('rubr_item_selected'); 
  $(this).addClass('rubr_item_selected'); 
  $('.rubr_item_third').hide(); 
  $('.parent_'+idx).show();
});
//===============================================================================================





















$(".openmesdialog").click(function() {
  var to = $(this).attr('touser');
  var username = $(this).attr('username');
  var from = $(this).attr('fromuser');

  
  $('#talkusername').html(username);
  $('#frame_message_dlg_user').attr('src','/account/messagelist/'+from);
  $('#inp__obj_to_user').val(from);
  $('#messageModal').modal('show');
});



$("#ddmenucat").load("/ajax/fn__get_category_tree_list");









});
