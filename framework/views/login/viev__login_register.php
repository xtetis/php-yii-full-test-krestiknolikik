


<form method="POST" enctype="multipart/form-data">
<div style="padding:5px;">
<table style="width:100%; margin-top:20px;">
  <tr>
    <td colspan="3" style="padding-bottom:5px;">
      <div  style="border-bottom:1px #d3d7cf solid; height:19px;">
        <h1 style="font-size:20px; line-height:16px;">
          <?=fn__lng_txt('Регистрация нового пользователя')?>
        </h1>
      </div>
    </td>
  </tr>






<!--                          Email                                          
   - *********************************************************************** -->
  <tr>
    <td class="b_add_legend">
      Email <span title="<?=fn__lng_txt('Обязательное поле')?>">*</span>
    </td>
    <td class="td_b_add_input">
      <div class="form-group" style="">
        <input type="email" 
               class="form-control" 
               name="formdata[email]" 
               value="<?=$info['email']?>" 
               data-toggle="popover" 
               title="Email" 
               data-content="<?=fn__lng_txt('Укажите email. При регистрации Вы получите на этот email письмо с данными о регистрации.')?>"
               required>
        <div class="<?=((strlen($info['errform']['email']))?'form_element_error_message':
                                                            'form_element_message')?>">
           <?=((strlen($info['errform']['email']))?$info['errform']['email']:fn__lng_txt('Минимальное количество символов - 7'))?>
        </div>
      </div>
    </td>
  </tr>
<!-- *********************************************************************** -->
  
  


<!--                          Контактное лицо                                          
   - *********************************************************************** -->
  <tr>
    <td class="b_add_legend">
      <?=fn__lng_txt('Контактное лицо')?> <span title="<?=fn__lng_txt('Обязательное поле')?>">*</span>
    </td>
    <td class="td_b_add_input">
      <div class="form-group" style="">
        <input type="text" 
               class="form-control" 
               name="formdata[username]" 
               value="<?=$info['username']?>" 
               data-toggle="popover" 
               title="<?=fn__lng_txt('Контактное лицо')?>" 
               data-content="<?=fn__lng_txt('Укажите имя, которое будет указано в ваших объявлениях.')?>"
               required>
        <div class="<?=((strlen($info['errform']['username']))?'form_element_error_message':
                                                               'form_element_message')?>">
          <?=((strlen($info['errform']['username']))?$info['errform']['username']:
                                                     fn__lng_txt('Минимальное количество символов - 4'))?>
        </div>
      </div>
    </td>
  </tr>
<!-- *********************************************************************** -->
  
  


<!--                          Пароль                                          
   - *********************************************************************** -->
  <tr>
    <td class="b_add_legend">
      <?=fn__lng_txt('Пароль')?>  <span title="<?=fn__lng_txt('Обязательное поле')?>">*</span>
    </td>
    <td class="td_b_add_input">
      <div class="form-group" style="">
        <input type="password" 
               class="form-control" 
               name="formdata[pass]" 
               value="<?=$info['pass']?>" 
               data-toggle="popover" 
               title="<?=fn__lng_txt('Пароль')?>" 
               data-content="<?=fn__lng_txt('Укажите пароль не меньше 6 символов.')?>"
               required>
        <div class="<?=((strlen($info['errform']['pass']))?'form_element_error_message':
                                                           'form_element_message')?>">
          <?=((strlen($info['errform']['pass']))?$info['errform']['pass']:fn__lng_txt('Длина пароля не меньше 6 символов'))?>
        </div>
      </div>
    </td>
  </tr>
<!-- *********************************************************************** -->






<!--                          Подтверждение пароля                                          
   - *********************************************************************** -->
  <tr>
    <td class="b_add_legend">
      <?=fn__lng_txt('Подтверждение пароля')?> <span title="<?=fn__lng_txt('Обязательное поле')?>">*</span>
    </td>
    <td class="td_b_add_input">
      <div class="form-group" style="">
        <input type="password" 
               class="form-control" 
               name="formdata[passdouble]" 
               value="<?=$info['passdouble']?>"
               data-toggle="popover" 
               title="<?=fn__lng_txt('Подтверждение пароля')?>" 
               data-content="<?=fn__lng_txt('Для регистрации повторите введите пароль')?>"
               required>
        <div class="<?=((strlen($info['errform']['passdouble']))?'form_element_error_message':
                                                                 'form_element_message')?>">
          <?=((strlen($info['errform']['passdouble']))?$info['errform']['passdouble']:
                                                       fn__lng_txt('Введите пароль повторно'))?>
        </div>
      </div>
    </td>
  </tr>
<!-- *********************************************************************** -->





<script>
$(function() {
  // Handler for .ready() called.
  $('[data-toggle="popover"]').popover({trigger : 'hover focus'});
});
</script>

















  
  
  
  <tr>
    <td></td>
    <td style="padding-top:20px;">
    <input type="submit" class="btn btn-primary btn-large" name="sbm" value="Регистрация"> 
    </td>
    <td></td>
  </tr>
</table>
</div>
<br>
<br>
<br>
<br>
</form>



