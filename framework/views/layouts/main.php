<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\assets\AppAsset;


/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<html lang="<?= Yii::$app->language ?>">
  <meta charset="<?= Yii::$app->charset ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?=fn__lng_txt('Крестики-нолики')?></title>

  <script type="text/javascript" src="/themes/components/jquery/jquery-2.1.3.min.js"></script>
  <script type="text/javascript" src="/themes/components/bootstrap/js/bootstrap.min.js"></script>  
  <link rel="stylesheet" href="/themes/components/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/themes/components/bootstrap/css/bootstrap-theme.min.css">


  <base href="http://<?=$_SERVER['HTTP_HOST']?>/">

  
  <link href="/themes/board/img/favicon/favicon_32_32.png" rel="shortcut icon" type="image/x-icon" />
  
  <link rel="stylesheet" href="/themes/board/css/common.css">
  <link rel="stylesheet" href="/themes/board/css/struct.css">
  <link rel="stylesheet" href="/themes/board/css/content.css">

  
  
<script>
function createCookie(name, value, days) {
    var expires;

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
}
</script>

    
  
  
  
  


</head>
<body>
  <script src="/themes/board/js/bootstrap_hover_dropdown.js"></script>

  <table class="maxtable struct_maintable">
    <tr>
      <td colspan="3" style="height:5px;"></td>
    </tr>    
    <tr>
      <td>&nbsp;</td>
      <td style="width:1000px;">
        <table class="maxtable struct_maintable_1">
          <tr>
            <td class="struct_maintable_1_header">
              <table class="maxtable">
                <tr>
                  <td class="td_logo">
                    <a href="/">
                      <div id="logodiv">
                        <img src="/themes/board/img/ico/logo.jpg" style="height:70px;">
                      </div>
                    </a>
                  </td>
                  <td style="padding-left:30px;">
                    <table class="maxtable">
                      <tr>
                        <td>
                          <?=fn__get_user_menu();?>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td style="width:270px;">
                    <?=fn__get_lang_menu()?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="vertical-align:top;">





   <?php echo $content; ?>
   
   

































   
   
   
            </td>
          </tr>
        </table>
      </td>
      <td>&nbsp;</td>
    </tr>    
    <tr>
      <td colspan="3" style="height:5px;">
      
      </td>
      
      
    </tr>    
  </table>
<div id="hiddendiv">
</div>
<?if(fn__get_user_id()):?>
<script>
  setInterval(function(){$("#hiddendiv").load("/site/activity");}, 5000);
</script>
<?endif;?>
  

  
</body>

</html>
