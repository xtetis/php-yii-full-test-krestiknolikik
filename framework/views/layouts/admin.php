<?php
header('Content-Type: text/html; charset=utf-8');
//namespace app\views\layouts;

use app\assets\AppAsset;
use yii\helpers\Html;
use app\models\LibTemplate;
use app\models\LibSettings;
use app\components\AdminTopMenu;


AppAsset::register($this);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>



<!--********************************************* Favicon ************************************** -->
<link rel="shortcut icon" href="<?=fn__get_template_filename('img/ico/favicon.png')?>" />
<!--******************************************** /Favicon ************************************** -->


<script src="/themes/xtetis/components/jquery/jquery-2.1.1.min.js"></script>

<!--****************************************** Bootstrap *************************************** -->
<link rel="stylesheet" type="text/css" href="/themes/xtetis/components/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/themes/xtetis/components/bootstrap/css/bootstrap-theme.min.css" />
<script src="/themes/xtetis/components/bootstrap/js/bootstrap.min.js"></script>
<!--****************************************** /Bootstrap ************************************** -->




<!--**************************************** Постраничная навигация **************************** -->
    <link rel="stylesheet" type="text/css" href="/themes/xtetis/components/paginator3000/paginator3000.css" />
    <script type="text/javascript" src="/themes/xtetis/components/paginator3000/paginator3000.js"></script>
<!--*************************************** /Постраничная навигация **************************** -->



<!--**************************************** Мои настройки и скрипты *************************** -->
<script type="text/javascript" src="/themes/xtetis/components/ckeditor/ckeditor.js"></script>
<link rel="stylesheet" type="text/css" href="/themes/xtetis/backend/css/admin.css" />
<script src="/themes/xtetis/backend/js/xtetis.js"></script>
<!--*************************************** /Мои настройки и скрипты *************************** -->




<title>Админка</title>
</head>
<body>
<table class="maxtable">
  <tr>
    <td style="height:30px; background-color:#fce94f;">
      <? // $this->widget('application.components.backend.AdminTopMenu'); ?>
      <?= AdminTopMenu::widget() ?>
    </td>
  </tr>
  <tr>
    <td style="vertical-align: top; padding-top: 10px; background-color: rgba(243, 243, 243, 1);">
      <div style="width: 1100px; margin: 0 auto;">
        <div class="panel panel-default">
          <div class="panel-body">
            <?php echo $content; ?>
          </div>
        </div>
      </div>
    </td>
  </tr>
  <tr>
    <td style="height:60px; background-color: rgba(243, 243, 243, 1);">
      <table class="maxtable">
        <tr>
          <td style="width:200px;">
            <img src="/themes/xtetis/backend/img/logo/XtetisCMS_logo.png" 
                 style="height: 60px; padding: 10px;">
          </td>
          <td style="text-align: center; font-size: 13px; color: #808080;">
            <div>
                Версия: <b><a href="/admin/help#section-8"><?=fn__get_setting('key__system_version')?></a></b>

            </div>
          </td>
          <td style="width: 230px; color: #808080; font-size: 13px; padding-top: 10px;">
              <div class="muted">Copyright © XtetisCMS 2012 - <?=date("Y")?></div>
              <a href="/admin/help">Документация</a>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<div id="clearcache">
  <div class="alert alert-info" role="alert" id="clearcache_alert"
       style="position: absolute; top: 40px; padding: 3px; right: 3px; display:none;">
  <img src="/themes/xtetis/backend/img/ico/wait_progress.gif" style="width: 16px;">
  Очистка кеша
  </div>
  <div id="clearcache1">
  </div>

  <script>
    $('#clearcache_alert').show();
    $("#clearcache1").load("/site/mod_cache/all", function() {
      $('#clearcache_alert').hide();
     });
  </script>

</div>

</body>
</html>
