<?
use app\components\AdminDirectoryTop;
$params['function_name']=$info['table'];
$params['title']='Страницы';
if ($info['command']=='edit'){
    $params['show_view']=true;
    $params['show_link']='/page/'.$info["id"];
   }
echo AdminDirectoryTop::widget(array('params'=>$params)) 
?>













<!-- ******************************************************************************************* -->
<?php if ($info['command']=='select') echo $info['select_table'].$info['pagination']; ?>
<!-- ******************************************************************************************* -->


















<!-- ******************************************************************************************* -->
<?php if (($info['command']=='edit')||($info['command']=='create')):?>
<form method="post" style="padding-top:10px;">

  <div class="form-group">
    <label>Название</label>
    <input type="text" 
           class="form-control" 
           maxlength="200" 
           name="formdata[name]" 
           value="<?=$info['name']?>">
  </div>
  
  <div class="checkbox" style="width: 400px;">
    <label>
      <input type="checkbox" 
             name="formdata[published]" 
             value="1" 
             <?=(($info["published"])?'checked="checked"':'')?>>
      Опубликовано
    </label>
  </div>
  
  <div class="checkbox" style="width: 400px;">
    <label>
      <input type="checkbox" 
             name="formdata[richedit]" 
             value="1" 
             <?=(($info["richedit"])?'checked="checked"':'')?>>
      Редактор
    </label>
  </div>
  
  <div class="checkbox" style="width: 400px;">
    <label>
      <input type="checkbox" 
             name="formdata[eval]" 
             value="1" 
             <?=(($info["eval"])?'checked="checked"':'')?>>
      Выполнить
    </label>
  </div>
  
  <div class="checkbox" style="width: 400px;">
    <label>
      <input type="checkbox" 
             name="formdata[index]" 
             value="1" 
             <?=(($info["index"])?'checked="checked"':'')?>>
      Индексировать
    </label>
  </div>
  
  <div class="checkbox" style="width: 400px;">
    <label>
      <input type="checkbox" 
             name="formdata[allsites]" 
             value="1" 
             <?=(($info["allsites"])?'checked="checked"':'')?>>
      Для всех сайтов
    </label>
  </div>
  
  <div class="form-group">
    <label>Title</label>
    <input type="text" 
           class="form-control" 
           maxlength="200" 
           name="formdata[title]" 
           value="<?=$info['title']?>">
  </div>
  
  <div class="form-group">
    <label>Description</label>
    <input type="text" 
           class="form-control" 
           maxlength="200" 
           name="formdata[description]" 
           value="<?=$info['description']?>">
  </div>
  
  <div class="form-group">
    <label>Текст статьи</label>
    <textarea class="<?=(($info["richedit"])?'mceEditor':'')?> form-control" 
              name="formdata[about]" 
              style="height:300px;" ><?=$info["about"]?></textarea>
  </div>
    
  <div class="form-group" style="padding-top:20px;">
  <input type="submit" 
         name="sbm" 
         onclick="return correct_submit()" 
         value="Сохранить" 
         class="btn btn-primary" />
  </div>
</form>
<?endif;?>
<!-- ******************************************************************************************* -->
