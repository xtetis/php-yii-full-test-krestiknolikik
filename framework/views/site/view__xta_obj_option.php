<?
use app\components\AdminDirectoryTop;
$params['function_name']=$info['table'];
$params['title']='Опции объявлений';
echo AdminDirectoryTop::widget(array('params'=>$params)) 
?>







<!-- ******************************************************************************************* -->
<?php if ($info['command']=='select') echo $info['select_table'].$info['pagination']; ?>
<!-- ******************************************************************************************* -->










<!-- ******************************************************************************************* -->
<?php if (($info['command']=='edit')||($info['command']=='create')):?>

<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li class="<?=((!$info['active_tab'])?'active':'')?>">
        <a href="#tab0" role="tab" data-toggle="tab">Опция</a></li>
    <?php if ($info['command']=='edit'):?>
    <li class="<?=(($info['active_tab']==1)?'active':'')?>">
        <a href="#tab1" role="tab" data-toggle="tab">Возможные значения</a></li>
    <li class="<?=(($info['active_tab']==2)?'active':'')?>">
        <a href="#tab2" role="tab" data-toggle="tab">Доступно для категорий</a></li>
    <?endif;?>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane <?=((!$info['active_tab'])?'active':'')?>" id="tab0">
      <form method="post" style="padding-top:10px;">  

        <div class="form-group">
          <label>Тип опции</label>
          <select name="formdata[option_type]" class="form-control" id="inp__option_type">
            <option value="0" <?=(($info['option_type']==0)?'selected':'')?>>Select</option>
            <option value="1" <?=(($info['option_type']==1)?'selected':'')?>>Text</option>
          </select>
        </div>

        <div class="form-group">
          <label>Название</label>
          <input type="text" 
                 class="form-control" 
                 maxlength="200" 
                 name="formdata[name]" 
                 value="<?=$info['name']?>">
        </div>
        
        
        <div class="form-group">
          <label>Описание</label>
          <input type="text" 
                 class="form-control" 
                 maxlength="200" 
                 name="formdata[description]" 
                 value="<?=$info['description']?>">
        </div>


        <div class="form-group" id="row__inp__isnumber">
          <label>Только цифры
            <input type="checkbox" 
                   maxlength="200" 
                   name="formdata[isnumber]" 
                   value="1"
                   <?=(($info['isnumber'])?'checked':'')?>>
          </label>
        </div>

  
        <div class="form-group" style="padding-top:20px;">
        <input type="submit" 
               name="sbm" 
               value="Сохранить" 
               class="btn btn-primary" />
        </div>

      </form>
    </div>
    
    <?php if ($info['command']=='edit'):?>
    <div role="tabpanel" class="tab-pane  <?=(($info['active_tab']==1)?'active':'')?>" id="tab1">

      <div>
        <table class="items table table-bordered table-hover">
          <thead>
            <tr>
              <th style="width:50px;">ID</th>
              <th>Значение</th>
              <th style="width:60px;"></th>
            </tr>
          </thead>
          <tbody>
            <?=$info['values_items']?>
          </tbody>
        </table>
      </div>
      <div>
        <form method="post">
        
          <input name="formdata[do]" id="do" type="hidden" value="add" />
          <input name="formdata[id_value]" id="id_value" type="hidden" value="0" />
          
          <div class="form-group">
            <label>Значение</label>
            <input type="text" 
                   class="form-control" 
                   maxlength="200" 
                   id="value_zn" 
                   name="formdata[value]" 
                   value="<?=$info['value']?>">
          </div>
          
          <div class="form-group" style="padding-top:20px;">
            <input type="submit" 
                   name="sbm_value" 
                   id="sbm_value"
                   value="Добавить" 
                   class="btn btn-primary" />
          </div>

        </form>
      </div>


    </div>
    <!-- ******************************************************************* -->
    <div role="tabpanel" class="tab-pane <?=(($info['active_tab']==2)?'active':'')?>" id="tab2">
      <form method="post" >
        <?=$info['category_tree']?>
        <div class="form-group" style="padding-top:20px;">
        <input type="submit" 
               name="sbm__opt_in_cat" 
               value="Сохранить" 
               class="btn btn-primary" />
        </div>
      </form>
    </div>
    <?endif;?>
  </div>

</div>

<?endif;?>
<!-- ******************************************************************************************* -->








<script>
$("#inp__option_type").change(function() {
  if (Number($(this).val())){
     $('#row__inp__isnumber').show();
     }else{
     $('#row__inp__isnumber').hide();
     }
});

$("#inp__option_type").change();
</script>

























<?
//******************************************************************************
if (($action=='update')||($action=='create')){


if (strlen($info['error'])){
$form.='
<div class="alert in alert-block fade alert-error">
<a class="close" data-dismiss="alert">×</a>
<strong> '.$info['error'].'</strong>
</div>';
}


$form.='


<style>
  label{
   font-weight:bold;
  }
  
  textarea{
    width:100%;
    height:300px;
  }
</style>
<form method="post" >
<table>

  <tr>
    <td>
  <label for="formdata[id_obj_option_type]">Тип опции</label>
  <select name="formdata[id_obj_option_type]">
    '.$info['id_obj_option_type'].'
  </select>
    </td>
  </tr>



  <tr>
    <td>
  <label for="formdata[introtext]">Подробное описание</label>
  <textarea style="width:90%; height:100px;" maxlength="300" name="formdata[introtext]">'.$info["introtext"].'</textarea>
    </td>
  </tr>
    
  <tr>
    <td>
  <label for="formdata[options]">Опции элемента</label>
  <input style="width:500px;" maxlength="200" name="formdata[options]" type="text" value="'.$info["options"].'" />
    </td>
  </tr>


  
  <tr>
    <td>
  <label for="formdata[convertype]">Тип данных</label>
  <input style="width:500px;" maxlength="200" name="formdata[convertype]" type="text" value="'.$info["convertype"].'" />
    </td>
  </tr>
  

</table>
    <br>
    

<br>
<input type="submit" name="sbm" value="Сохранить" />
</form>
';



?>
<br>
<?
 
$_tab_values=array();
if ($action=='update'){

$_values_content='
<div>
  <table class="items table table-bordered">
<thead>
<tr>
<th style="width:50px;">ID</th>
<th>Значение</th>
<th style="width:50px;">Изменить</th>
<th style="width:50px;">Удалить</th>
</tr>
</thead>
<tbody>
  '.$info['values_items'].'
</tbody>
  </table>
</div>
<div>
<form method="post" >
  <table>
    <input name="formdata[do]" id="do" type="hidden" value="add" />
    <input name="formdata[id_value]" id="id_value" type="hidden" value="0" />
    <tr>
    <td>
     <label for="formdata[value]">Значение</label>
     <input style="width:500px;" maxlength="200" id="value_zn" name="formdata[value]" type="text" value="'.$info["value"].'" />
    </td>
  </tr>

</table>
    <br>
  <input type="submit" name="sbm_value" id="sbm_value" value="Добавить" />
</form>
</div>
';
$_tab_values=array('label'=>'Возможные значения', 'content'=>$_values_content);






$_in_categories_content='
<form method="post" >
'.$info['category_tree'].'
    <br>
  <input type="submit" name="sbm__opt_in_cat" value="Изменить" />
</form>
';


$_tab_in_categories=array('label'=>'Доступно для категорий', 'content'=>$_in_categories_content);
}
  
  $this->widget('bootstrap.widgets.TbTabs', array(
    'type'=>'tabs',
    'placement'=>'above', 
    'tabs'=>array(
     array('label'=>$info['title_one'], 'content'=>$form, 'active'=>true),
     $_tab_values,
     $_tab_in_categories,
    ),
)); 
}
//******************************************************************************
?>


