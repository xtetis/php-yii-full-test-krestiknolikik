<script type="text/javascript" src="/themes/board/js/game.js"></script>

<div style="padding:10px;">
<?if(strlen($info['message'])):?>
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <?=$info['message']?>
</div>
<?endif;?>






<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?=fn__lng_txt('Список активных пользователей')?></h3>
  </div>
  <div class="panel-body" id="last_active_users">
    <?=fn__get_active_users();?>
  </div>
</div>


<script>
  setInterval(function(){$("#last_active_users").load("/site/getlastactiveasers");}, 5000);
</script>


</div>




<div style="display:none;" id="tempdiv"></div>








<div class="modal fade" id="game_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close modalclose" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?=fn__lng_txt('Игра')?></h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default modalclose"><?=fn__lng_txt('Прекратить игру')?></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
