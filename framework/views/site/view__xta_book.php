<?
use app\components\AdminDirectoryTop;
$params['function_name']=$info['table'];
$params['title']='Книги';
echo AdminDirectoryTop::widget(array('params'=>$params)) 
?>
	<script type="text/javascript" src="/themes/components/fancybox/source/jquery.fancybox.pack.js "></script>
	<link rel="stylesheet" type="text/css" href="/themes/components/fancybox/source/jquery.fancybox.css" media="screen" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="/themes/board/js/jquery-ui.js"></script>






<!-- ******************************************************************************************* -->
<?php if ($info['command']=='select'):?>

<form method="post" action="/site/xta_book">
        <div class="form-group col-md-2">
          <label>Автор</label>
          <select name="filter[id_author]" class="form-control">
          <?=$info["id_author"]?>
          </select>
        </div>
        
        <div class="form-group  col-md-2">
          <label>Название книги</label>
          <input type="text" 
                 class="form-control" 
                 maxlength="200" 
                 name="filter[name]" 
                 value="<?=$_COOKIE['filter__name']?>">
        </div>
        
        <div class="form-group  col-md-4" >
          <label style="display: block;">Дата выхода книги</label>
          <input type="text" 
                 class="form-control datepicker filter__datestart" 
                 maxlength="200"
                 style="display: inline; width: 32%;"
                 name="filter[datestart]" 
                 value="<?=$_COOKIE['filter__datestart']?>" readonly>
           до
          <input type="text" 
                 class="form-control datepicker filter__dateend" 
                 maxlength="200"
                 style="display: inline; width: 32%;"
                 name="filter[dateend]" 
                 value="<?=$_COOKIE['filter__dateend']?>" readonly>
        </div>

        <div class="form-group  col-md-2">
          <label>&nbsp;</label>
          <input type="submit" 
                 class="form-control" 
                 maxlength="200" 
                 name="sbmfilter"
                 value="Искать">
        </div>
        
        
        <div class="form-group  col-md-2">
          <label>&nbsp;</label>
          <input type="submit" 
                 class="form-control" 
                 maxlength="200" 
                 name="clrfilter"
                 value="Сбросить">
        </div>


<script>
$(function() {
    $( ".datepicker" ).datepicker();
    $( ".datepicker" ).datepicker( "option", "dateFormat", "yy-mm-dd");
    <?=(isset($_COOKIE['filter__datestart'])?'$(".filter__datestart").val(\''.$_COOKIE['filter__datestart'].'\');':'')?>
    <?=(isset($_COOKIE['filter__dateend'])?'$(".filter__dateend").val(\''.$_COOKIE['filter__dateend'].'\');':'')?>
});
</script>
        
        
</form>


<form method="post">

</form>



<? echo $info['select_table'].$info['pagination']; ?>

<div class="modal fade" id="modalview">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalbooktitle" style="">Название книги</h4>
      </div>
      <div class="modal-body">
        <div id="modalbookcontent" style="max-height:300px; overflow-y:scroll;"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
$(function() {
  $(".viewbookcl").click(function() {
    var idx = $(this).attr('idx');

    $.get("/site/xta_book/view/"+idx, function( data ) {
        var obj = jQuery.parseJSON(data);
        $('#modalbookcontent').html(obj.about);
        $('#modalbooktitle').html(obj.name);  
        $('#modalview').modal('show')
      });
    
    
  });
  
  $(".fancybox").fancybox();
});
</script>

<?endif;?>
<!-- ******************************************************************************************* -->












<!-- ******************************************************************************************* -->
<?php if (($info['command']=='edit')||($info['command']=='create')):?>

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
  <li class="<?=((!$info["activetab"])?'active':'')?>"><a href="#tab1" role="tab" data-toggle="tab">
    <span class="glyphicon glyphicon-pencil"></span>
    Книга</a></li>
  <?php if ($info['command']=='edit'):?>
    <li class="<?=(($info["activetab"]==1)?'active':'')?>"><a href="#tab2" role="tab" data-toggle="tab">
        <span class="glyphicon glyphicon-picture"></span>
        Альбом</a></li>
  <?endif;?>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane <?=((!$info["activetab"])?'active':'')?>" id="tab1">
    <form method="post" style="padding-top:10px;">

        <div class="form-group">
          <label>Автор</label>
          <select name="formdata[id_author]" class="form-control">
          <?=$info["id_author"]?>
          </select>
        </div>

        <div class="form-group">
          <label>Название книги</label>
          <input type="text" 
                 class="form-control" 
                 maxlength="200" 
                 name="formdata[name]" 
                 value="<?=$info['name']?>">
        </div>
        
        <div class="form-group">
          <label>Содержимое книги</label>
          <textarea class="form-control ckeditor" id="zzz" name="formdata[about]" ><?=$info['about']?></textarea>
        </div>
        
        <div class="form-group">
          <label>Дата выхода</label>
          <input type="text" 
                 class="form-control datepicker" 
                 maxlength="200"
                 name="formdata[date]" 
                 value="<?=$info['date']?>" readonly>
        </div>

        <div class="form-group" style="padding-top:20px;">
        <input type="submit" 
               name="sbm" 
               value="Сохранить" 
               class="btn btn-primary" />
        </div>



        <script>
        $(function() {
            $( ".datepicker" ).datepicker();
            $( ".datepicker" ).datepicker( "option", "dateFormat", "yy-mm-dd");
            $( ".datepicker" ).val('<?=$info['date']?>');
        });
        </script>
    </form>
 </div>
  <?php if ($info['command']=='edit'):?>
    <div class="tab-pane <?=(($info["activetab"]==1)?'active':'')?>" id="tab2" style="padding-top:10px;">
      <div class="alert alert-success" 
           style="display:<?=((strlen($info['imgmessage']))?'block':'none')?>">
        <a class="close" data-dismiss="alert">×</a>
        <?=$info["imgmessage"]?>
      </div>
      
      <form method="POST" ENCTYPE="multipart/form-data"> 
        <div class="panel panel-default" style="width: 400px;">
          <div class="panel-body">
            <button class="btn btn-primary" 
                    type="submit" name="imgupload" style="float:right; margin-top: 8px;">
                    Загрузить</button>
            <label for="imgadmupl">Добавить изображение</label>
           <input name="imgadmupl[]" type="file" multiple>
         </div>
       </div>
      </form>
      <?=$info["adminalbum"]?>
    </div>
  <?endif;?>
</div>



<?endif;?>
<!-- ******************************************************************************************* -->





