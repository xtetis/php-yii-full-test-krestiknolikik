  <input type="hidden" id="inp_current_game" value="<?=$info['id']?>">
<div style="padding:10px;">
<?if(strlen($info['titlemessage'])):?>
<script>
  $('.modal-title').html('<?=$info['titlemessage']?>');
</script>
<?endif;?>

<div id="gamealert">
  <input type="hidden" id="inp_step_status" value="<?=$info['candostep']?>">
  <div id="step_status_1" class="step_status" 
       style="<?=(($info['candostep'])?'display:block':'')?>"><?=fn__lng_txt('Ваш ход')?></div>
  <div id="step_status_0" class="step_status" 
       style="<?=(($info['candostep'])?'':'display:block')?>"><?=fn__lng_txt('Ходит ваш оппонент')?></div>
</div>

<div>
<table class="table table-bordered table_game">
  <tr>
    <td onclick="set_step(<?=$info['id']?>,1);" id="cell_1"><?=$info['pos1']?></td>
    <td onclick="set_step(<?=$info['id']?>,2);" id="cell_2"><?=$info['pos2']?></td>
    <td onclick="set_step(<?=$info['id']?>,3);" id="cell_3"><?=$info['pos3']?></td>
  </tr>
  <tr>
    <td onclick="set_step(<?=$info['id']?>,4);" id="cell_4"><?=$info['pos4']?></td>
    <td onclick="set_step(<?=$info['id']?>,5);" id="cell_5"><?=$info['pos5']?></td>
    <td onclick="set_step(<?=$info['id']?>,6);" id="cell_6"><?=$info['pos6']?></td>
  </tr>
  <tr>
    <td onclick="set_step(<?=$info['id']?>,7);" id="cell_7"><?=$info['pos7']?></td>
    <td onclick="set_step(<?=$info['id']?>,8);" id="cell_8"><?=$info['pos8']?></td>
    <td onclick="set_step(<?=$info['id']?>,9);" id="cell_9"><?=$info['pos9']?></td>
  </tr>
</table>
</div>


<div id="stepalert">
  <div id="step_alert_2" style="display:none; color:red; font-weight:bold; text-align:center;">
       <?=fn__lng_txt('Ячейка занята')?></div>
  <div id="step_alert_3" style="display:none; color:red; font-weight:bold; text-align:center;">
       <?=fn__lng_txt('Шагов больше не осталось. Игра закончилась.')?></div>
  <div id="step_alert_4" style="display:none; color:red; font-weight:bold; text-align:center;">
       <?=fn__lng_txt('Вы победили')?></div>
  <div id="step_alert_5" style="display:none; color:red; font-weight:bold; text-align:center;">
       <?=fn__lng_txt('Вы проиграли')?></div>
</div>


</div>

<?if(!isset($_GET['notimer'])):?>
<script>
  var interval__check_go_step =setInterval(function(){
      check_go_step(<?=$info['id']?>);
    }, 3000);
</script>
<?endif;?>

