<?
use app\components\AdminDirectoryTop;




$params['function_name']='xta_settings';
$params['title']='Настройки сайта';
//$this->widget('application.components.backend.AdminDirectoryTop',array('params'=>$params)); 
echo AdminDirectoryTop::widget(array('params'=>$params)) 
?>







<!-- ******************************************************************************************* -->
<?php if ($info['command']=='select') echo $info['select_table']; ?>
<!-- ******************************************************************************************* -->











<!-- ******************************************************************************************* -->
<?php if (($info['command']=='edit')||($info['command']=='create')):?>

<?if(strlen($info["error"]))echo '<div class="alert alert-danger" role="alert">'.$info["error"].'</div>';?>

<form method="post" style="padding-top:10px;">

    <div class="form-group">
      <label>Категория</label>
      <select name="formdata[category]" class="form-control">
      <?=$info["category"]?>
      </select>
    </div>

    <div class="form-group">
      <label>Название (Ключ)</label>
      <input type="text" 
             class="form-control" 
             maxlength="200" 
             name="formdata[name]" 
             value="<?=$info['name']?>">
    </div>

    <div class="form-group">
      <label>Описание</label>
      <input type="text" 
             class="form-control" 
             maxlength="200" 
             name="formdata[title]" 
             value="<?=$info['title']?>">
    </div>
  
    <div class="checkbox" style="width: 400px;">
      <label>
        <input type="checkbox" 
               name="formdata[multisite]" 
               value="1" 
               onClick="confirmChange(this)"
               <?=(($info["multisite"])?'checked="checked"':'')?>>
        Настройка для всех сайтов
      </label>
    </div>
    
    <div class="form-group">
      <label>Значение</label>
      <textarea class="form-control" name="formdata[value]"><?=$info["value"]?></textarea>
    </div>
    
    
    
    
    <div class="form-group">
      <label>Значение по-умолчанию</label>
      <textarea class="form-control" name="formdata[default]"><?=$info["default"]?></textarea>
    </div>
      
      
    <div class="form-group" style="padding-top:20px;">
    <input type="submit" 
           name="sbm" 
           onclick="return correct_submit()" 
           value="Сохранить" 
           class="btn btn-primary" />
    </div>
</form>



<script>
function confirmChange(element) {
    var ch=element.checked;
    if (ch){
      if (confirm("Вы подтверждаете удаление уникальных настроек этого ключа для каждого сайта\n и создание одинаковых настроек для всех сайтов?")) {
          element.checked=true;
      } else {
          element.checked=false;
      }
    }
}
</script>
<?endif;?>
<!-- ******************************************************************************************* -->








<?
//******************************************************************************
if (($action=='update')||($action=='create')){


if (strlen($info["error"])){
  $info["error"]='
<div class="alert in alert-block fade alert-error"><a class="close" data-dismiss="alert">×</a><strong>Ошибка </strong>'.$info["error"].'</div>
  ';
}

$form='
<style>
  label{
   font-weight:bold;
  }
  
  textarea{
    width:100%;
    height:300px;
  }
</style>



'.$info["error"].'

<form method="post" >
<script>
function confirmChange(element) {
    var ch=element.checked;
    if (ch){
      if (confirm("Вы подтверждаете удаление уникальных настроек этого ключа для каждого сайта\n и создание одинаковых настроек для всех сайтов?")) {
          element.checked=true;
      } else {
          element.checked=false;
      }
    }

}
</script>



<label for="formdata[id_settings_category]">Категория</label>
<select name="formdata[id_settings_category]">
'.LibSql::fn__get_select_by_sql_i_tpl('SELECT * FROM `xta_settings_category` ',
                              '<option value="+id+" +default+>+name+</option>',
                              $info['id_settings_category']).'
</select>

<table>
  <tr>
    <td>
  <label for="formdata[name]">Название (Ключ)</label>
  <input style="width:500px;" maxlength="90" name="formdata[name]" type="text" value="'.$info["name"].'" />		
    </td>   
    <td>
	   <input type="checkbox" '.$info["multisite"].' name="formdata[multisite]" value="1" onClick="confirmChange(this)">
	   Настройка для всех сайтов
    </td>   
  </tr>  
</table>
    
<table>
  <tr>
    <td>
      
      
      <label for="formdata[title]" class="formdata_title_input">Пояснение</label>
      <input style="width:500px;" maxlength="200" name="formdata[title]" type="text" value="'.$info["title"].'" class="formdata_title_input" />
      <br>
      

      
      <br>
      <label for="formdata[value]">Значение</label>
      <textarea style="width:90%; height:120px;" name="formdata[value]">'.$info["value"].'</textarea>
    </td> 
  </tr>
</table>

<br>
<input type="submit" name="sbm" value="Сохранить" />
</form>
';















if ($action=='update'){
$form1='
<form method="post" >
<label for="formdata[type]">Тип</label>
<select maxlength="200" name="formdata[type]">
  <option value="On/Off" '.(($info["type"]=="On/Off")?'selected="selected"':'').'>On/Off</option>
  <option value="Select" '.(($info["type"]=="Select")?'selected="selected"':'').'>Select</option>
  <option value="Text" '.(($info["type"]=="Text")?'selected="selected"':'').'>Text</option>
</select>


<br>
<label for="formdata[available]">Возможные значения</label>
<textarea style="width:90%; height:120px;" name="formdata[available]">'.$info["available"].'</textarea>
<br>
<input type="submit" name="sbm_options" value="Сохранить" />
</form>
';
}else{
$form1='';
}


?>
<br>
<?
  
  $this->widget('bootstrap.widgets.TbTabs', array(
    'type'=>'tabs',
    'placement'=>'above', 
    'tabs'=>array(
     array('label'=>'Настройка', 'content'=>$form, 'active'=>$info["activetab"]),
     array('label'=>'Опции', 'content'=>$form1),
    ),
)); 
}
//******************************************************************************
?>


