<?
use app\components\AdminDirectoryTop;
$params['function_name']=$info['table'];
$params['title']='Категории объявлений';
echo AdminDirectoryTop::widget(array('params'=>$params)) 
?>


<script>
	CKEDITOR.replace('textareaId', {
	});
</script>



<!-- ******************************************************************************************* -->
<?php if ($info['command']=='select') echo $info['select_table'].$info['pagination']; ?>
<!-- ******************************************************************************************* -->
























<!-- ******************************************************************************************* -->
<?php if (($info['command']=='edit')||($info['command']=='create')):?>
<form method="post" style="padding-top:10px;">

  <?if (strlen($info['error'])):?>
    <div class="alert in alert-block fade alert-error">
      <a class="close" data-dismiss="alert">×</a>
      <strong> <?=$info['error']?></strong>
    </div>';
  <?endif;?>



  <div class="form-group">
    <label>Родительская категория</label>
    <select name="formdata[id_parent]" class="form-control">
    <?=$info["id_parent"]?>
    </select>
  </div>
  

  <div class="form-group">
    <label>Название</label>
    <input type="text" 
           class="form-control" 
           maxlength="200" 
           name="formdata[name]" 
           value="<?=$info['name']?>">
  </div>
  
  
  
  <div class="form-group">
    <label>H1</label>
    <input type="text" 
           class="form-control" 
           maxlength="200" 
           name="formdata[hone]" 
           value="<?=$info['hone']?>">
  </div>
  
  
  
  <div class="form-group">
    <label>Title страницы</label>
    <input type="text" 
           class="form-control" 
           maxlength="200" 
           name="formdata[title]" 
           value="<?=$info['title']?>">
  </div>
  
  
  
  <div class="form-group">
    <label>Description страницы</label>
    <input type="text" 
           class="form-control" 
           maxlength="200" 
           name="formdata[description]" 
           value="<?=$info['description']?>">
  </div>
  
  
  
  <div class="form-group">
    <label>Ссылка на изображение (начинать со слеша)</label>
    <input type="text" 
           class="form-control" 
           maxlength="200" 
           name="formdata[img]" 
           value="<?=$info['img']?>">
  </div>
    
  <div class="form-group">
    <label>Сеотекст</label>
    <textarea class="form-control" class="ckeditor" name="formdata[seotext]"><?=$info["seotext"]?></textarea>
  </div>
  
    

  
  <div class="form-group" style="padding-top:20px;">
  <input type="submit" 
         name="sbm" 
         value="Сохранить" 
         class="btn btn-primary" />
  </div>

</form>
<?endif;?>
<!-- ******************************************************************************************* -->

