<?
use app\components\AdminDirectoryTop;
$params['function_name']=$info['table'];
$params['title']='Документация';
echo AdminDirectoryTop::widget(array('params'=>$params)) 
?>










<!-- ******************************************************************************************* -->
<?php if ($info['command']=='select') echo $info['select_table'].$info['pagination']; ?>
<!-- ******************************************************************************************* -->













<!-- ******************************************************************************************* -->
<?php if (($info['command']=='edit')||($info['command']=='create')):?>
<form method="post" style="padding-top:10px;">

    <div class="form-group">
      <label>Название категории</label>
      <input type="text" 
             class="form-control" 
             maxlength="200" 
             name="formdata[name]" 
             value="<?=$info['name']?>">
    </div>
    
    <div class="form-group">
      <label>Позиция</label>
      <input type="text" 
             class="form-control intval" 
             maxlength="2" 
             name="formdata[pos]" 
             value="<?=$info['pos']?>">
    </div>
    
    <div class="form-group">
      <label>Текст стравки</label>
      <textarea class="form-control" id="zzz" name="formdata[about]" ><?=$info['about']?></textarea>
    </div>
  
    <div class="form-group" style="padding-top:20px;">
    <input type="submit" 
           name="sbm" 
           value="Сохранить" 
           class="btn btn-primary" />
    </div>
</form>

<script>
CKEDITOR.config.contentsCss = ['/themes/xtetis/components/bootstrap/css/bootstrap.min.css'];
CKEDITOR.replace('zzz');
</script>
<?endif;?>
<!-- ******************************************************************************************* -->
