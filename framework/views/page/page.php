<?php
//SEO
//******************************************************************************
$this->pageTitle=$info["title"];
Yii::app()->clientScript->registerMetaTag($info["description"], 'Description');
//******************************************************************************
?>


<? include('left_column.inc.php'); ?> 
<!-- ********** content ********** --> 
<table style="height:100%;">
 <tr>
  <td style="vertical-align:top; height:100%;">
    <h1 style="font-size:22px; margin:0px;" class="breadcrumbs breadcrumb"><?=$info["h1"];?></h1>
    <div class="only_content" style="padding:8px;">
      <?=$info["about"];?>
    </div>  
  </td>
 </tr>
 <tr>
  <td style="height:20px;">
    <div class="breadcrumbs breadcrumb" style="margin:0px;">
      <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" style="display:inline;">
        <a href="/" itemprop="url">
          <span itemprop="title"><?=fn__get_setting('site_name');?></span>
        </a> ›
      </div>  
      <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" style="display:inline;">
        <a href="/page/<?=$info["id"];?>" itemprop="url">
          <span itemprop="title"><?=$info["h1"];?></span>
        </a>
      </div>  
    </div>
  </td>
 </tr>
</table>
<!-- ********** /content ********** -->   
  </td>                
</tr> 
</table>




