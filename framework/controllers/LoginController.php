<?php



namespace app\controllers;


use Yii;
use yii\web\Controller;
include_once(__DIR__ . '/../libraries/tetislib.php');


class LoginController extends Controller {
  public $enableCsrfValidation = false;
public $depends = [
            //'yii\web\JqueryAsset',
            //'yii\bootstrap\BootstrapAsset'
            'yii\bootstrap\BootstrapPluginAsset'
        ];  
	public function actionIndex(){
	  include('login/index.php');
	}
  
  
	public function actionLogout()
	{	
	   $to_url='/';
	   if (isset($_GET['to'])){$to_url='/'.$_GET['to'];}
	   fn__set_logout($to_url);
	}	
	
	
	public function actionForgotpass(){
	  include('login/forgotpass.php');
	}
	
	public function actionRegister(){
	  include('login/register.php');
	}
	
}
