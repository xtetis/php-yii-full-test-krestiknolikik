<?php

$data["info"]["command"]=$command;
$table = 'xta_city';
$data["info"]["table"] = $table;
  
  
  
  
  
  
  

//Показываем список сайтов
//**************************************************************************************************
if ($command=='select'){
  
  $count_records = fn__get_count_by_where($table);
  $maxpage=ceil($count_records/10);
  $page=fn__get_correct_page($maxpage);
  $data["info"]["pagination"].=fn__get_pagination('/admin/'.$table.'?page=', $maxpage, $page);
  
  $sql="SELECT 
          `".$table."`.* ,
          `xta_region`.`name` as 'regionname'
        FROM `".$table."` 
        LEFT JOIN `xta_region` on 
                  `xta_region`.`id` = `".$table."`.`id_region`
        LIMIT ".(10*($page-1)).",10";
	  $reader =\Yii::$app->db->createCommand($sql)->query(); 
	  $data['info']['select_table']='<table class="table table-bordered table-hover" style="margin-bottom:0px;">
	  <tr style="background:#d3d7cf;">
	    <th style="width: 30px;">#</th>
	    <th>Город</th>
	    <th>Область</th>
	    <th style="width: 60px;"></th>
	  </tr>
	  ';
	  foreach ($reader as $row){
	  $data['info']['select_table'].='
	  <tr>
	    <td>'.$row['id'].'</td>
	    <td>'.$row['name'].'</td>
	    <td>'.$row['regionname'].'</td>
	    <td>
	      <div class="btn-group btn-group-sm btn-group-select_item">
	        <a href="/admin/'.$table.'/edit/'.$row['id'].'" 
	           class="btn btn-default"
	           data-toggle="tooltip" 
	           data-placement="top" 
	           title="Редактировать запись">
	          <span class="glyphicon glyphicon-edit"></span></a>
	        <a href="/admin/'.$table.'/delete/'.$row['id'].'" class="btn btn-default" 
	           onclick="return confirmDelete();"
	           data-toggle="tooltip" 
	           data-placement="top" 
	           title="Удалить запись">
	           <span class="glyphicon glyphicon-trash"></span></a>
	      </div>
	    </td>
	  </tr>
	  ';
	  }
	  $data['info']['select_table'].='</table>
	  <div style="text-align:right;">
 	  Показано '.($count_records?((10*($page-1))+1):$count_records).' - 
	  '.(($count_records>(10*($page))?(10*($page)):$count_records)).' 
	  из '.$count_records.' записей</div>';
}
//**************************************************************************************************


  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
// Создаем сайт
//**************************************************************************************************
if ($command=='create'){
   if(isset($_POST['sbm']))
     {
       $_name = sql_valid(strip_tags($_POST['formdata']['name']));
       $_id_region=intval($_POST['formdata']['id_country']);
       $sql="INSERT INTO `".$table."`(`id_region`,`name`) 
             VALUES (".$_id_region.",".$_name.")";
       \Yii::$app->db->createCommand($sql)->execute(); 
       header("Location: /admin/".$table."/edit/".fn__get_max_table_id($table));
       exit();
    }
    
  $data['info']['id_region']=fn__get_select_by_sql_i_tpl(
      'SELECT * FROM `xta_region`','<option value="+id+" +default+>+name+</option>');
}
//**************************************************************************************************




















// Редактируем сайт
//**************************************************************************************************
if ($command=='edit'){

   if(isset($_POST['sbm']))
     {
       $_name = sql_valid(strip_tags($_POST['formdata']['name']));
       $_id_region=intval($_POST['formdata']['id_region']);
       $sql="UPDATE `".$table."` SET 
               `name`=".$_name.",
               `id_region` = ".$_id_region."
             WHERE `id`=".$id;
       \Yii::$app->db->createCommand($sql)->execute(); 
     }

   $sql="SELECT * FROM `".$table."` WHERE `id` = ".$id;
   $row =\Yii::$app->db->createCommand($sql)->queryOne(); 
   $data['info']['name']=$row['name'];
   $data['info']['id_region']=fn__get_select_by_sql_i_tpl(
        'SELECT * FROM `xta_region`','<option value="+id+" +default+>+name+</option>',
        $row['id_region']);
   
}
//**************************************************************************************************











// Удаляем сайт
//**************************************************************************************************
if ($command=='delete'){
  fn__del_record_by_id($table,$id);
  header("Location: /admin/".$table);
  exit();
}
//**************************************************************************************************









echo $this->render('view__'.$table,$data); 
