<?php



$data["info"]["command"]=$command;
$table = 'xta_obj_category';
$data["info"]["table"] = $table;



//Показываем список категорий
//**************************************************************************************************

if ($command=='select'){
	  $sql="SELECT * FROM `$table` WHERE `id_parent` = 0 ORDER BY `name` ASC";
	  $reader =\Yii::$app->db->createCommand($sql)->query(); 
	  $select='
	  <br>
	  <table class="items table table-striped table-bordered table-condensed" style="height:10px;">
    <thead>
      <tr>
       <th id="yw0_c0">#</th>
       <th id="yw0_c1" colspan="2">Название</th>
       <th class="button-column" id="yw0_c2">&nbsp;</th>
      </tr>
    </thead>
	  ';
	  
	  foreach ($reader as $row){
      $select.='
      <tr onClick="$(\'.level_2_'.$row['id'].'\').toggle();">
        <td style="width: 30px;">'.$row['id'].'</td>
        <td colspan="3">'.$row['name'].' ('.fn__get_count_by_where('xta_obj_category','`id_parent` = '.$row['id']).')</td>
        <td style="width: 60px;">
         <a title="Update" rel="tooltip" href="/admin/xta_obj_category/edit/'.$row['id'].'">
           <span class="glyphicon glyphicon-edit"></span>
         </a>    
         <a title="Delete" rel="tooltip" 
            onclick="return confirmDelete();"
            href="/admin/xta_obj_category/delete/'.$row['id'].'">
           <span class="glyphicon glyphicon-trash"></span>
         </a>        
        </td>
      </tr>

      ';

	    $sql="SELECT * FROM `$table` WHERE `id_parent` = ".$row['id']." ORDER BY `name` ASC";
	    $reader1 =\Yii::$app->db->createCommand($sql)->query(); 
	    foreach ($reader1 as $row1){
	    $select.='
        <tr class="level_2_'.$row['id'].'" style="display:none;"  onClick="$(\'.level_3_'.$row1['id'].'\').toggle();">
          <td style="width: 30px;">'.$row1['id'].'</td>
          <td style="width:30px;">&nbsp;</td>
          <td  colspan="2">'.$row1['name'].' ('.fn__get_count_by_where('xta_obj_category','`id_parent` = '.$row1['id']).')</td>
          <td>
           <a title="Update" rel="tooltip" href="/admin/xta_obj_category/edit/'.$row1['id'].'">
             <span class="glyphicon glyphicon-edit"></span>
           </a>        
           <a title="Delete" rel="tooltip" 
              onclick="return confirmDelete();"
              href="/admin/xta_obj_category/delete/'.$row1['id'].'">
             <span class="glyphicon glyphicon-trash"></span>
           </a>  
          </td>
        </tr>
	    ';
	    
	    
	    
	    
	        $sql="SELECT * FROM `$table` WHERE `id_parent` = ".$row1['id']." ORDER BY `name` ASC";
	        $reader2 =\Yii::$app->db->createCommand($sql)->query(); 
	        foreach ($reader2 as $row2){
	        $select.='
            <tr class="level_3_'.$row1['id'].'" style="display:none;">
              <td style="width: 30px;">'.$row2['id'].'</td>
              <td style="width:30px;">&nbsp;</td>
              <td style="width:30px;">&nbsp;</td>
              <td>'.$row2['name'].'</td>
              <td>
               <a title="Update" rel="tooltip" href="/admin/xta_obj_category/edit/'.$row2['id'].'">
                 <span class="glyphicon glyphicon-edit"></span>
               </a>
               <a title="Delete" rel="tooltip" 
                  onclick="return confirmDelete();"
                  href="/admin/xta_obj_category/delete/'.$row2['id'].'">
                 <span class="glyphicon glyphicon-trash"></span>
               </a>
              </td>
            </tr>
	        ';
	        }
	    
	    
	    }
      
	  }
	  $select.='</table>';


	  $data['info']['select_table']=$select;

	  $data['info']['id_parent']=fn__get_admin_obj_parent_category_options(0);
}
//**************************************************************************************************












  
  
  
  
  
  
  
  
// Создаем
//**************************************************************************************************
if ($command=='create'){
   //----------------------------------------------------------------------------
   if(isset($_POST['sbm']))
     {
       $_name = sql_valid(strip_tags($_POST['formdata']['name']));
       $_hone = sql_valid(strip_tags($_POST['formdata']['hone']));
       $_img = sql_valid(strip_tags($_POST['formdata']['img']));
       $_id_parent = intval($_POST['formdata']['id_parent']);
       $_title = sql_valid(strip_tags($_POST['formdata']['title']));
       $_description = sql_valid(strip_tags($_POST['formdata']['description']));
       $_seotext = sql_valid($_POST['formdata']['seotext']);
       
       $sql="INSERT INTO `".$table."`(
                 `id_parent`,
                 `name`, 
                 `hone`, 
                 `title`, 
                 `description`, 
                 `seotext`, 
                 `img`
                 ) 
              VALUES (
                 ".$_id_parent.",
                 '".$_name."',
                 '".$_hone."',
                 '".$_title."',
                 '".$_description."',
                 '".$_seotext."',
                 '".$_img."'
                 )        
                ";
       \Yii::$app->db->createCommand($sql)->execute(); 
       
       header("Location: /admin/".$table."/edit/".fn__get_max_table_id($table));
       exit();
    }   
}
//**************************************************************************************************



















// Редактируем
//**************************************************************************************************
if ($command=='edit'){

 
   //----------------------------------------------------------------------------
   if(isset($_POST['sbm']))
     {
       $_name = sql_valid(strip_tags($_POST['formdata']['name']));
       $_hone = sql_valid(strip_tags($_POST['formdata']['hone']));
       $_img = sql_valid(strip_tags($_POST['formdata']['img']));
       $_id_parent = intval($_POST['formdata']['id_parent']);
       $_title = sql_valid(strip_tags($_POST['formdata']['title']));
       $_description = sql_valid(strip_tags($_POST['formdata']['description']));
       $_seotext = sql_valid($_POST['formdata']['seotext']);
                
          
          
       if ($_id_parent==0){
           $_new_level = 0;
          }elseif(fn__get_field_val_by_id('xta_obj_category','id_parent',$_id_parent)==0){
            $_new_level = 1;
          }elseif(fn__get_field_val_by_id('xta_obj_category','id_parent',fn__get_field_val_by_id('xta_obj_category','id_parent',$_id_parent))==0){
            $_new_level = 2;
          }
       
       $_has_children = fn__get_count_by_where('xta_obj_category','`id_parent` = '.$id);
       $_has_sub_children = fn__get_count_by_where('xta_obj_category','`id_parent` in (SELECT `id` FROM `xta_obj_category` WHERE `id_parent` = '.$id.') ');
       
       if (!$_has_children){
         $error=false;
       }elseif($_has_children&&(!$_has_sub_children)&&($_new_level<2)){
         $error=false;
       }elseif($_has_sub_children&&($_new_level==0)){
         $error=false;
       }else{
         $error=true;
         $data['info']['error']='Нельзя выбирать эту родительскую категорию';
       }
       
       
                
                
        $sql="
              UPDATE `".$table."` SET 
                `id_parent`=".$_id_parent.",
                `name`='".$_name."',
                `hone`='".$_hone."',
                `title`='".$_title."',
                `description`='".$_description."',
                `seotext`='".$_seotext."',
                `img`='".$_img."'
              WHERE 
                `id`=".$id;
                
        if ($error){
          $sql="
                UPDATE `".$table."` SET 
                  `name`='".$_name."',
                  `hone`='".$_hone."',
                  `title`='".$_title."',
                  `description`='".$_description."',
                  `seotext`='".$_seotext."',
                  `img`='".$_img."'
                WHERE 
                  `id`=".$id;
        }
        \Yii::$app->db->createCommand($sql)->execute(); 
     }
   //----------------------------------------------------------------------------


   
   $sql="SELECT * FROM `".$table."` WHERE `id` = ".$id;
   $row =\Yii::$app->db->createCommand($sql)->queryOne(); 
   
   $data['info']['id']=$row['id'];
   $data['info']['name']=$row['name'];
   $data['info']['hone']=$row['hone'];
   $data['info']['title']=$row['title'];
   $data['info']['description']=$row['description'];
   $data['info']['seotext']=$row['seotext'];
   $data['info']['img']=$row['img'];
   $data['info']['id_parent']=fn__get_admin_obj_parent_category_options($row['id_parent']);
   $data['info']['mid_parent']=$row['id_parent'];
}
//**************************************************************************************************



















// Удаляем
//**************************************************************************************************
if ($command=='delete'){
  fn__del_record_by_id($table,$id);
  header("Location: /admin/".$table);
  exit();
}
//**************************************************************************************************

















echo $this->render('view__'.$table,$data);
