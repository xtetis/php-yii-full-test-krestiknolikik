<?php

  $data["info"]["command"]=$command;
  
  












//Показываем список настроек
//**************************************************************************************************
if ($command=='select'){
  $tabshead='';
  $tabsbody='';
  $sql="SELECT * FROM `xta_setting_category` ";
  $reader2 =\Yii::$app->db->createCommand($sql)->query(); 
  foreach ($reader2 as $row2){
	  $ret = '';
	  $sql="SELECT `id`,`name`, `title`,`value` FROM `xta_setting`
	        WHERE `id_setting_category` = ".$row2['id']."
	        ORDER BY `xta_setting`.`name` ASC ";
	  $reader =\Yii::$app->db->createCommand($sql)->query(); 
	  $select=array();
	  $data['info']['backgroundempty']='#fff';
	  foreach ($reader as $row){
	       $row1=$row;
	       $_val=unserialize($row['value']);
	       $isvalue='Да';
	       if (isset($_val[0])){
	        if ($_val[0]==''){$row1['notempty']='Нет'; $isvalue='Нет';}
	        $multisite='Да';
	       }else{
	         $multisite='Нет';
	         if ((!isset($_val[Libsetting::fn__get_site_id()]))||($_val[Libsetting::fn__get_site_id()]=='')){
	             $isvalue='Нет';
	            }
	       }
	       
	     $ret.='
	     <tr>
	       <td>'.$row['id'].'</td>
	       <td>'.$row['name'].'</td>
	       <td>'.$row['title'].'</td>
	       <td>'.$multisite.'</td>
	       <td style="background:'.(($isvalue=='Нет')?'#FBB2B2':'#C3FB8C').';">'.$isvalue.'</td>
	       <td>
           <div class="btn-group btn-group-sm btn-group-select_item"> 
            <a href="/admin/xta_setting/edit/'.$row['id'].'" 
               class="btn btn-default"
               data-toggle="tooltip" 
               data-placement="top" 
               title="Редактировать запись">
              <span class="glyphicon glyphicon-edit"></span></a>
           </div>
	       </td>
	     </tr>
	     ';
	       
	     }
	  $ret='<table class="table table-bordered table-hover" style="margin-bottom:0px;">
	     <tr style="background:#d3d7cf;">
	       <th style="width: 10px;">ID</th>
	       <th style="width: 300px;">Имя</th>
	       <th>Описание</th>
	       <th style="width: 10px;">Мультисайт</th>
	       <th style="width: 10px;">Значение</th>
	       <th style="width: 40px;"></th>
	     </tr>
	     '.$ret.'
	     </table>';
	  
	  $tabsbody.='<div class="tab-pane '.(strlen($tabsbody)?'':'active').
               '" id="tab_'.$row2['id'].'">'.$ret.'</div>';
    $tabshead.='<li '.(strlen($tabshead)?'':' class="active"').
               '><a href="#tab_'.$row2['id'].'" role="tab" data-toggle="tab">'.$row2['name'].'</a></li>';
  }
  $tabshead = '<br><ul class="nav nav-tabs" role="tablist">'.$tabshead.'</ul>';
  $tabsbody = '<div class="tab-content">'.$tabsbody.'</div>';
  $data['info']['select_table'] = $tabshead.$tabsbody;
}
//**************************************************************************************************




















// Создаем настройку
//**************************************************************************************************
if ($command=='create'){
   if(isset($_POST['sbm'])){
       $_name = sql_valid(mb_substr(strip_tags($_POST['formdata']['name']),0,90,'utf-8'));
       $_title = sql_valid(mb_substr(strip_tags($_POST['formdata']['title']),0,200,'utf-8'));
       $_value = $_POST['formdata']['value'];
       $_default = sql_valid($_POST['formdata']['default']);
       $_multisite = intval($_POST['formdata']['multisite']);
       $_id_setting_category=intval($_POST['formdata']['category']);
       
       $count = fn__get_count_by_where('xta_setting','`name` = '.$_name);
       if ($count){
         $data['info']['name']=strip_tags(mb_substr($_POST['formdata']['name'],0,90,'utf-8'));
         $data['info']['title']=strip_tags($_POST['formdata']['title']);
         $data['info']['value']=$_POST['formdata']['value'];
         $data['info']['default']=$_POST['formdata']['default'];
         $data['info']['multisite']= $_multisite;
         $data['info']['error']= 'Такой ключ уже есть. Запись не сохранена.';
       }else{
         if ($_multisite){
            $_setvalue['0']=$_value;
            }else{
            $_setvalue[Libsetting::fn__get_site_id()]=$_value;
            }
         $_value=sql_valid(serialize($_setvalue));
     	   $sql="INSERT INTO `xta_setting`(`id_setting_category`,`name`, `title`, `value`,`default`) 
     	         VALUES (
     	         ".$_id_setting_category.",
     	         ".$_name.",
     	         ".$_title.",
     	         ".$_value.",
     	         ".$_default."
     	         )";
     	   \Yii::$app->db->createCommand($sql)->execute();
         header("Location: /admin/xta_setting/edit/".fn__get_max_table_id('xta_setting'));
         exit();
       }
   }
  $data['info']['category']=fn__get_select_by_sql_i_tpl(
    'SELECT * FROM `xta_setting_category` ',
    '<option value="+id+" +default+>+name+</option>');
}
//******************************************************************************


















// Редактируем настройку
//******************************************************************************
if ($command=='edit'){
  //----------------------------------------------------------------------------
  if(isset($_POST['sbm'])){
     $_name = sql_valid(strip_tags($_POST['formdata']['name']));
     $_title = sql_valid(strip_tags($_POST['formdata']['title']));
     $_value = $_POST['formdata']['value'];
     $_default = sql_valid($_POST['formdata']['default']);
     $_newmultisite = intval($_POST['formdata']['multisite']);
     $_id_setting_category=intval($_POST['formdata']['category']);

     $sql="SELECT `value` FROM `xta_setting` WHERE `id` = '".$id."'";
     $row =\Yii::$app->db->createCommand($sql)->queryOne();
     $_oldvalue=unserialize($row['value']);
     $_oldmultisite=isset($_setvalue[0]);
     if ($_newmultisite&&$_oldmultisite){
        $_newsetvalue['0']=$_value;
        $_new_value=serialize($_newsetvalue);
     }
     if ($_newmultisite&&(!$_oldmultisite)){
        $_newsetvalue['0']=$_value;
        $_new_value=serialize($_newsetvalue);
     }
     if ((!$_newmultisite)&&($_oldmultisite)){
        $_newsetvalue[Libsetting::fn__get_site_id()]=$_value;
        $_new_value=serialize($_newsetvalue);
     }
     
     if ((!$_newmultisite)&&(!$_oldmultisite)){
        $_setvalue = $_oldvalue;
        $_setvalue[Libsetting::fn__get_site_id()]=$_value;
        $_new_value=serialize($_setvalue);
     }       
     $_new_value=sql_valid($_new_value);
    
 
     $count = fn__get_count_by_where('xta_setting','`name` = '.$_name." AND `id`<>".$id);
     if ($count){
         $data['info']['error']= 'Такой ключ уже есть. Настройка не сохранена.';
         $data['info']['name']=strip_tags(mb_substr($_POST['formdata']['name'],0,90,'utf-8'));
         $data['info']['title']=strip_tags(mb_substr($_POST['formdata']['title'],0,200,'utf-8'));
         $data['info']['value']=$_POST['formdata']['value'];
         $data['info']['default']=$_POST['formdata']['default'];
         $data['info']['multisite']= $_newmultisite;
        }
        
      if (!strlen( $data['info']['error'])){
          $sql="UPDATE `xta_setting` SET 
                  `id_setting_category`=".$_id_setting_category.",
                  `name`=".$_name.",
                  `title`=".$_title.",
                  `value`=".$_new_value.",
                  `default`=".$_default."
                WHERE 
                  `id`=".$id;
          \Yii::$app->db->createCommand($sql)->execute(); 
      }
   }
  //----------------------------------------------------------------------------











  //----------------------------------------------------------------------------
  if (!strlen($data['info']['error'])){ 
     $sql="SELECT * FROM `xta_setting` WHERE `id` = ".$id;
     $row =\Yii::$app->db->createCommand($sql)->queryOne(); 
     
     $data['info']['id']=$row['id'];
     $data['info']['name']=$row['name'];
     $data['info']['title']=$row['title'];
     $data['info']['default']=$row['default'];
     $data['info']['category']=fn__get_select_by_sql_i_tpl(
        'SELECT * FROM `xta_setting_category` ',
        '<option value="+id+" +default+>+name+</option>',
        $row['id_setting_category']);
     
     $_setvalue=unserialize($row['value']);
     $_multisite=isset($_setvalue[0]);
     $data['info']['multisite']= $_multisite;
     
     if ($_multisite){
       $data['info']['value']=$_setvalue[0];
     }else{
       if (isset($_setvalue[Libsetting::fn__get_site_id()])){
        $data['info']['value']=$_setvalue[Libsetting::fn__get_site_id()];
       }else{
        $data['info']['value']='';
       }
     }
   }
  //----------------------------------------------------------------------------
}
//******************************************************************************


















echo $this->render('view__xta_setting',$data); 
