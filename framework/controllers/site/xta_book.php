<?php

$data["info"]["command"]=$command;
$table = 'xta_book';
$data["info"]["table"] = $table;
  
if (isset($_POST)&&(count($_POST))){
 //  print_r($_POST); 
//   exit;
   }
//print_r($_COOKIE);
  
// ФИльтруем
//**************************************************************************************************
if (isset($_POST['sbmfilter'])){
   $_name = trim($_POST['filter']['name']);
   $_id_author = intval($_POST['filter']['id_author']);
   $_datestart = trim($_POST['filter']['datestart']);
   $_datestart = (!strlen($_datestart))?"":date('Y-m-d',strtotime($_datestart));
   $_dateend = trim($_POST['filter']['dateend']);
   $_dateend = (!strlen($_dateend))?"":date('Y-m-d',strtotime($_dateend));
   setcookie("filter__name", $_name, time()+3600);
   setcookie("filter__id_author", $_id_author, time()+3600);
   setcookie("filter__datestart", $_datestart, time()+3600);
   setcookie("filter__dateend", $_dateend, time()+3600);
   header("Location: /site/".$table);
   exit();
   }
//**************************************************************************************************
  
  
  
// 
//**************************************************************************************************
if (isset($_POST['clrfilter'])){
   setcookie("filter__name", '', time()-3600);
   setcookie("filter__id_author", '', time()-3600);
   setcookie("filter__datestart", '', time()-3600);
   setcookie("filter__dateend", '', time()-3600);
   header("Location: /site/".$table);
   exit();
   }
//**************************************************************************************************
  





//**************************************************************************************************
if (isset($_GET['sortby'])){
   if (in_array($_GET['sortby'],array('id','name','id_author','date','date_create'))){
      $cursort =$_COOKIE['order__sort'];
      if ($cursort==$_GET['sortby']){
         if ($_COOKIE['order__ord']=='DESC'){
            setcookie("order__ord", 'ASC', time()+3600); 
            }else{
            setcookie("order__ord", 'DESC', time()+3600); 
            }
         }else{
         setcookie("order__ord", 'DESC', time()+3600);
         setcookie("order__sort", $_GET['sortby'], time()+3600);
         }
      }
   header("Location: /site/".$table);
   exit();
   }
//**************************************************************************************************












//Показываем список сайтов
//**************************************************************************************************
if ($command=='select'){

  $filters=array();
  $filter__dateend='';
  if (isset($_COOKIE['filter__dateend'])){
     $filter__dateend = date('Y-m-d',strtotime($_COOKIE['filter__dateend']));
     $filter__dateend=" `xta_book`.`date`<='".$filter__dateend."' ";
     $filters[]=$filter__dateend;
     }

  $filter__datestart='';
  if (isset($_COOKIE['filter__datestart'])){
     $filter__datestart = date('Y-m-d',strtotime($_COOKIE['filter__datestart']));
     $filter__datestart=" `xta_book`.`date`>='".$filter__datestart."' ";
     $filters[]=$filter__datestart;
     }

     
  $filter__id_author='';
  if (isset($_COOKIE['filter__id_author'])&&(intval($_COOKIE['filter__id_author']))){
     $filter__id_author = " `xta_book`.`id_author`= ".intval($_COOKIE['filter__id_author']);
     $filters[]=$filter__id_author;
     }
     
  $filter__name='';
  if (isset($_COOKIE['filter__name'])){
     $filter__name = " `xta_book`.`name` LIKE '%".$_COOKIE['filter__name']."%' ";
     $filters[]=$filter__name;
     }
     
  $filters = implode(' AND ',$filters);
     


  $sodtf['id']='';
  $sodtf['name']='';
  $sodtf['id_author']='';
  $sodtf['date']='';
  $sodtf['date_create']='';
  $sort = '';
  if ((isset($_COOKIE['order__sort']))&&
      (isset($_COOKIE['order__ord']))&&
      (in_array($_COOKIE['order__sort'],array('id','name','id_author','date','date_create')))&&
      (in_array($_COOKIE['order__ord'],array('ASC','DESC')))){
      $sort = ' ORDER BY `xta_book`.`'.$_COOKIE['order__sort'].'` '.$_COOKIE['order__ord'].' ';
      $sodtf[$_COOKIE['order__sort']] = '<span class="glyphicon glyphicon-arrow-down"></span>';
      if ($_COOKIE['order__ord']=='DESC'){
         $sodtf[$_COOKIE['order__sort']] = '<span class="glyphicon glyphicon-arrow-up"></span>';
         }
      }
  
  
  

  $count_records = fn__get_count_by_where($table,$filters);
  $maxpage=ceil($count_records/10);
  $page=fn__get_correct_page($maxpage);
  $data["info"]["pagination"]=fn__get_pagination('/admin/'.$table.'?page=', $maxpage, $page);

  $sql="SELECT 
          `".$table."`.* ,
          `xta_author`.`firstname`,
          `xta_author`.`lastname`
        FROM `".$table."` 
        LEFT JOIN `xta_author` on 
                  `xta_author`.`id` = `".$table."`.`id_author`
        ".((strlen($filters))?'WHERE '.$filters:'')."
        ".$sort."
        LIMIT ".(10*($page-1)).",10";

	  $reader =Yii::$app->db->createCommand($sql)->query(); 



	  $data['info']['select_table']='<table class="table table-bordered table-hover" style="margin-bottom:0px;">
	  <tr style="background:#d3d7cf;">
	    <th style="width: 30px;"><a href="/site/xta_book?sortby=id"># '.$sodtf['id'].'</a></th>
	    <th><a href="/site/xta_book?sortby=name">Название '.$sodtf['name'].'</a></th>
	    <th>Обложка</th>
 	    <th><a href="/site/xta_book?sortby=id_author">Автор '.$sodtf['id_author'].'</a></th>
	    <th><a href="/site/xta_book?sortby=date">Дата выхода книги '.$sodtf['date'].'</a></th>
	    <th><a href="/site/xta_book?sortby=date_create">Дата добавления '.$sodtf['date_create'].'</a></th>
	    <th style="width: 90px;"></th>
	  </tr>
	  ';
	
	  foreach ($reader as $row){
	  $mainimg = fn__get_album_mainimg_src($row['id_album']);
	  if ($mainimg){
	  $mainimg = '
<a class="fancybox" href="/'.$mainimg.'">
<img src="/'.$mainimg.'" style="max-width:40px; max-height:40px;">
</a>
	  ';}
	  

	  
	  $data['info']['select_table'].='
	  <tr>
	    <td>'.$row['id'].'</td>
	    <td>'.$row['name'].'</td>
	    <td>'.$mainimg.'</td>
	    <td>'.$row['firstname'].' '.$row['lastname'].'</td>
	    <td>'.$row['date'].'</td>
	    <td>'.$row['date_create'].'</td>
	    <td>
	      <div class="btn-group btn-group-sm btn-group-select_item">
	        <a href="/site/'.$table.'/edit/'.$row['id'].'" 
	           class="btn btn-default"
	           data-toggle="tooltip" 
	           data-placement="top" 
	           title="Редактировать запись">
	          <span class="glyphicon glyphicon-edit"></span></a>
	        <a href="javascript:void(0)" 
	           idx="'.$row['id'].'"
	           class="btn btn-default viewbookcl"
	           data-toggle="tooltip" 
	           data-placement="top" 
	           title="Просмотреть запись">
	          <span class="glyphicon glyphicon-eye-open"></span></a>
	        <a href="/site/'.$table.'/delete/'.$row['id'].'" class="btn btn-default" 
	           onclick="return confirmDelete();"
	           data-toggle="tooltip" 
	           data-placement="top" 
	           title="Удалить запись">
	           <span class="glyphicon glyphicon-trash"></span></a>
	      </div>
	    </td>
	  </tr>
	  ';
	  }
	  
	  $data['info']['select_table'].='</table>
	  <div style="text-align:right;">
 	  Показано '.($count_records?((10*($page-1))+1):$count_records).' - 
	  '.(($count_records>(10*($page))?(10*($page)):$count_records)).' 
	  из '.$count_records.' записей</div>';
    $data['info']['id_author']=fn__get_select_by_sql_i_tpl(
        'SELECT * FROM `xta_author`','<option value="+id+" +default+>+firstname+ +lastname+</option>',
        ((isset($_COOKIE['filter__id_author'])?intval($_COOKIE['filter__id_author']):0)));
    $data['info']['id_author']='<option value="0">Не указано</option>'.$data['info']['id_author'];
}
//**************************************************************************************************


  
    
  
  
  
  
// Создаем 
//**************************************************************************************************
if ($command=='view'){
   $sql="SELECT * FROM `".$table."` WHERE `id` = ".$id;
   $row =\Yii::$app->db->createCommand($sql)->queryOne(); 
   $ret['name']=$row['name'];
   $ret['about']=$row['about'];
   echo json_encode($ret); exit;
}
//**************************************************************************************************
  
  
  
  
  
  
  
  
  
  
// Создаем 
//**************************************************************************************************
if ($command=='create'){
   if(isset($_POST['sbm']))
     {
       $_name = sql_valid(strip_tags($_POST['formdata']['name']));
       $_about = sql_valid(strip_tags($_POST['formdata']['about']));
       $_date = sql_valid(date('Y-m-d',strtotime($_POST['formdata']['date'])));
       $_id_author=intval($_POST['formdata']['id_author']);
       $sql="
       INSERT INTO `".$table."`(
           `id_author`, 
           `name`, 
           `date_create`, 
           `date_update`, 
           `date`, 
           `about`
       ) VALUES (
           $_id_author,
           ".$_name.",
           NOW(),
           NOW(),
           ".$_date.",
           ".$_about."
       );";
       \Yii::$app->db->createCommand($sql)->execute(); 
       header("Location: /site/".$table."/edit/".fn__get_max_table_id($table));
       exit();
    }
    
  $data['info']['id_author']=fn__get_select_by_sql_i_tpl(
      'SELECT * FROM `xta_author`','<option value="+id+" +default+>+firstname+ +lastname+</option>');
}
//**************************************************************************************************














// Устанавливаем заглавное изображение
//**************************************************************************************************
if (isset($_GET['def'])){
if (fn__image_exists(intval($_GET['def']))){
   if (fn__set_main_img_to_album(intval($_GET['def']),fn__get_book_album($id,'book_'.$id))){
       $data["info"]["imgmessage"]="Для альбома установлена 
                                    картинка по умолчанию #".intval($_GET['def']);
      }else{
        $data["info"]["imgmessage"]="Ошибка при установке картинки по умолчанию";
      }
   $data["info"]["activetab"]=1;
   }
}
//**************************************************************************************************







// Удаляем изображение
//**************************************************************************************************
if (isset($_GET['del'])){
if (fn__image_exists(intval($_GET['del']))){
  if (fn__del_img(intval($_GET['del']))){
    $data["info"]["imgmessage"]="Удалено изображение #".intval($_GET['del']);
  }else{
    $data["info"]["imgmessage"]="Ошибка при удалении картинки";
  }
  $data["info"]["activetab"]=1;
  }
}
//**************************************************************************************************














// Загружаем изображения в альбом
//**************************************************************************************************

if(isset($_POST['imgupload'])){
  if (fn__post_upload_img(fn__get_book_album($id,'book_'.$id),'imgadmupl')){
       $data["info"]["imgmessage"]="Изображения добавлены в альбом";
     }else{
       $data["info"]["imgmessage"]="Ошибка при загрузке изображений";
     }
    $data["info"]["activetab"]=1;
}
//**************************************************************************************************









// Редактируем 
//**************************************************************************************************
if ($command=='edit'){

   if(isset($_POST['sbm']))
     {
       $_name = sql_valid(strip_tags($_POST['formdata']['name']));
       $_about = sql_valid(strip_tags($_POST['formdata']['about']));
       $_date = sql_valid(date('Y-m-d',strtotime($_POST['formdata']['date'])));
       $_id_author=intval($_POST['formdata']['id_author']);
       $sql="UPDATE `".$table."` SET 
               `name`=".$_name.",
               `about`=".$_about.",
               `date`=".$_date.",
               `date_update`=NOW(),
               `id_author` = ".$_id_author."
             WHERE `id`=".$id;
       //echo $sql;
       \Yii::$app->db->createCommand($sql)->execute(); 
       header("Location: /site/".$table);
       exit();
     }

   $sql="SELECT * FROM `".$table."` WHERE `id` = ".$id;
   $row =\Yii::$app->db->createCommand($sql)->queryOne(); 
   $data['info']['name']=$row['name'];
   $data['info']['about']=$row['about'];
   $data['info']['date']=$_date = date('Y-m-d',strtotime($row['date']));
   $data['info']['id_author']=fn__get_select_by_sql_i_tpl(
        'SELECT * FROM `xta_author`','<option value="+id+" +default+>+firstname+ +lastname+</option>',
        $row['id_country']);
  $data["info"]["adminalbum"]=fn__get_admin_album(fn__get_book_album($id,'book_'.$id),
                                                  '/site/xta_book/edit/'.$id.'');
}
//**************************************************************************************************











// Удаляем сайт
//**************************************************************************************************
if ($command=='delete'){
  fn__del_record_by_id($table,$id);
  header("Location: /admin/".$table);
  exit();
}
//**************************************************************************************************





//	  echo 12354; exit;



echo $this->render('view__'.$table,$data); 
