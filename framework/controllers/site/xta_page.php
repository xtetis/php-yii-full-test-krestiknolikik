<?php
$data["info"]["command"]=$command;
$table = 'xta_page';
$data["info"]["table"] = $table;






//Показываем список cnраниц
//**************************************************************************************************
if ($command=='select'){
  
  $count_records = fn__get_count_by_where($table,'`id_site`='.fn__get_site_id().' or `id_site`=0');
  $maxpage=ceil($count_records/10);
  $page=fn__get_correct_page($maxpage);
  $data["info"]["pagination"].=fn__get_pagination('/admin/'.$table.'?page=', $maxpage, $page);
  
  $sql="SELECT 
        `$table`.* ,
        IF(`$table`.`published`=1,'Опубликовано','Не опубликовано') as 'publetter',
        IF(`$table`.`id_site`=0,'Мультисайтовая','Уникальная') as 'multisite'
         FROM `$table` 
        WHERE
         `id_site`=".fn__get_site_id()."
         OR
         `id_site`=0
        LIMIT ".(10*($page-1)).",10";
  $reader =\Yii::$app->db->createCommand($sql)->query(); 
  $data['info']['select_table']='<table class="table table-bordered table-hover" style="margin-bottom:0px;">
  <tr style="background:#d3d7cf;">
    <th style="width: 30px;">#</th>
    <th>Название</th>
    <th>Мультисайт</th>
    <th>Публикация</th>
    <th style="width: 60px;"></th>
  </tr>
  ';
	  foreach ($reader as $row){
	  $data['info']['select_table'].='
	  <tr>
	    <td>'.$row['id'].'</td>
	    <td>'.$row['name'].'</td>
	    <td>'.$row['publetter'].'</td>
	    <td>'.$row['multisite'].'</td>
	    <td>
	      <div class="btn-group btn-group-sm btn-group-select_item">
	        <a href="/admin/'.$table.'/edit/'.$row['id'].'" 
	           class="btn btn-default"
	           data-toggle="tooltip" 
	           data-placement="top" 
	           title="Редактировать запись">
	          <span class="glyphicon glyphicon-edit"></span></a>
	        <a href="/admin/'.$table.'/delete/'.$row['id'].'" class="btn btn-default" 
	           onclick="return confirmDelete();"
	           data-toggle="tooltip" 
	           data-placement="top" 
	           title="Удалить запись">
	           <span class="glyphicon glyphicon-trash"></span></a>
	      </div>
	    </td>
	  </tr>
	  ';
	  }
	  $data['info']['select_table'].='</table>
	  <div style="text-align:right;">
 	  Показано '.($count_records?((10*($page-1))+1):$count_records).' - 
	  '.(($count_records>(10*($page))?(10*($page)):$count_records)).' 
	  из '.$count_records.' записей</div>';
}
//**************************************************************************************************



























// Создаем страницу
//**************************************************************************************************
if ($command=='create'){
   $data['info']['published']=1;
   $data['info']['richedit']=1;
   $data['info']['eval']='';
   $data['info']['index']=1;
   
   //----------------------------------------------------------------------------
   if(isset($_POST['sbm'])){
       $_name = strip_tags($_POST['formdata']['name']);
       $_title = strip_tags($_POST['formdata']['title']);
       $_description = $_POST['formdata']['description'];
       $_about = sql_valid($_POST['formdata']['about']);
       $_published = intval($_POST['formdata']['published']);
       $_richedit = intval($_POST['formdata']['richedit']);
       $_eval = intval($_POST['formdata']['eval']);
       $_index = intval($_POST['formdata']['index']);
       $_allsites = intval($_POST['formdata']['allsites']);

       $_id_site =fn__get_site_id();
       if ($_allsites)$_id_site =0;
               
       $sql="INSERT INTO `$table`(
              `id_site`,
              `name`, 
              `title`,
              `description`, 
              `about`, 
              `published`, 
              `richedit`,
              `eval`, 
              `index`) 
             VALUES (
              '".$_id_site."',
              '".$_name."',
              '".$_title."',
              '".$_description."',
              '".$_about."',
              ".$_published.",
              ".$_richedit.",
              ".$_eval.",
              ".$_index.")";
       Yii::app()->db->createCommand($sql)->execute(); 
       header("Location: /admin/$table/edit/".fn__get_max_table_id($table));
       exit();
    }
    //----------------------------------------------------------------------------
 }
//**************************************************************************************************


















// Редактируем страницу
//**************************************************************************************************
if ($command=='edit'){
  
   //----------------------------------------------------------------------------
   if(isset($_POST['sbm'])){
       $_name = $_POST['formdata']['name'];
       $_title = $_POST['formdata']['title'];
       $_description = $_POST['formdata']['description'];
       $_about = sql_valid($_POST['formdata']['about']);
       $_published = intval($_POST['formdata']['published']);
       $_richedit = intval($_POST['formdata']['richedit']);
       $_eval = intval($_POST['formdata']['eval']);
       $_index = intval($_POST['formdata']['index']);
       $_allsites = intval($_POST['formdata']['allsites']);

       $_id_site =fn__get_site_id();
       if ($_allsites)$_id_site =0;
                
       $sql="UPDATE `$table` SET 
               `id_site` = ".$_id_site.",
               `name`='".$_name."',
               `title`='".$_title."',
               `description`='".$_description."',
               `about`='".$_about."',
               `published`=".$_published." ,
               `richedit`=".$_richedit." ,
               `eval`=".$_eval." ,
               `index`=".$_index." ,
               `editedon`= CURRENT_TIMESTAMP
             WHERE 
               `id`=".$id;
        Yii::app()->db->createCommand($sql)->execute(); 
     }
   //----------------------------------------------------------------------------


   
   $sql="SELECT * FROM `$table` WHERE `id` = ".$id;
   $row =Yii::app()->db->createCommand($sql)->queryRow(); 
   $data['info']['id']=$row['id'];
   $data['info']['name']=$row['name'];
   $data['info']['title']=$row['title'];
   $data['info']['description']=$row['description'];
   $data['info']['about']=$row['about'];
   $data['info']['richedit']=$row['richedit'];
   $data['info']['published']=$row['published'];
   $data['info']['eval']=$row['eval'];
   $data['info']['index']=$row['index'];
   $data['info']['allsites']=($row['id_site']==0);
 }
//**************************************************************************************************


















// Снимаем с публикации страницу
//**************************************************************************************************
if ($command=='delete')fn__publish_page($id,!fn__is_page_published($id));
//**************************************************************************************************













echo $this->render('view__'.$table,$data); 
