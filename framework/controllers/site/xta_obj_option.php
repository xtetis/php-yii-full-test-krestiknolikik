<?php



$data["info"]["command"]=$command;
$table = 'xta_obj_option';
$data["info"]["table"] = $table;


//Показываем список опций
//******************************************************************************

if ($command=='select'){

  $count_records = fn__get_count_by_where($table);
  $maxpage=ceil($count_records/10);
  $page=fn__get_correct_page($maxpage);
  $data["info"]["pagination"].=fn__get_pagination('/admin/'.$table.'?page=', $maxpage, $page);

  $sql="SELECT * FROM `$table` ";
  $reader =\Yii::$app->db->createCommand($sql)->query(); 
  $data['info']['select_table']='
  <br>
  <table class="items table table-striped table-bordered table-condensed table-hover" style="height:10px;">
    <tr>
      <td style="width: 30px; font-weight:bold;">ID</td>
      <td style=" font-weight:bold;">Тип</td>
      <td style=" font-weight:bold;">Название</td>
      <td style=" font-weight:bold;">Описание</td>
      <td style="width: 60px;"></td>
    </tr>
  ';
	  
  foreach ($reader as $row){
    $data['info']['select_table'].='
    <tr>
      <td>'.$row['id'].'</td>
      <td>'.(($row['option_type'])?'text':'select').'</td>
      <td>'.$row['name'].'</td>
      <td>'.$row['description'].'</td>
      <td>
	      <div class="btn-group btn-group-sm btn-group-select_item">
	        <a href="/admin/'.$table.'/edit/'.$row['id'].'" 
	           class="btn btn-default"
	           data-toggle="tooltip" 
	           data-placement="top" 
	           title="Редактировать запись">
	          <span class="glyphicon glyphicon-edit"></span></a>
	        <a href="/admin/'.$table.'/delete/'.$row['id'].'" class="btn btn-default" 
	           onclick="return confirmDelete();"
	           data-toggle="tooltip" 
	           data-placement="top" 
	           title="Удалить запись">
	           <span class="glyphicon glyphicon-trash"></span></a>
	      </div>
      </td>
    </tr>
    ';
	  }
	  $data['info']['select_table'].='</table>
	  <div style="text-align:right;">
 	  Показано '.($count_records?((10*($page-1))+1):$count_records).' - 
	  '.(($count_records>(10*($page))?(10*($page)):$count_records)).' 
	  из '.$count_records.' записей</div>';
}
//******************************************************************************




















// Создаем запись
//**************************************************************************************************
if ($command=='create'){
   if(isset($_POST['sbm']))
     {
       $_name = sql_valid($_POST['formdata']['name']);
       $_description = sql_valid($_POST['formdata']['description']);
       $_isnumber = intval($_POST['formdata']['isnumber']);
       $_option_type = intval($_POST['formdata']['option_type']);
       
       $sql="INSERT INTO `".$table."`(
               `name`,
               `description`,
               `isnumber`,
               `option_type`
             ) VALUES (
               '".$_name."',
               '".$_description."',
               '".$_isnumber."',
               '".$_option_type."'
             );";
       \Yii::$app->db->createCommand($sql)->execute(); 
       header("Location: /admin/".$table."/edit/".fn__get_max_table_id($table));
       exit();
    }
}
//**************************************************************************************************














// Редактируем запись
//**************************************************************************************************
if ($command=='edit'){

   //----------------------------------------------------------------------------
   if(isset($_POST['sbm'])){
       $_name = sql_valid($_POST['formdata']['name']);
       $_description = sql_valid($_POST['formdata']['description']);
       $_isnumber = intval($_POST['formdata']['isnumber']);
       $_option_type = intval($_POST['formdata']['option_type']);
                
       $sql="UPDATE `".$table."` SET 
               `option_type`=".$_option_type.",
               `name`=".$_name.",
               `description`=".$_description.",
               `isnumber`=".$_isnumber."
             WHERE 
               `id`=".$id;
       \Yii::$app->db->createCommand($sql)->execute(); 
     }
   //----------------------------------------------------------------------------
     




   // Устанавливаем данные о принадлежности опций к категориям
   //----------------------------------------------------------------------------
   if (isset($_POST['sbm__opt_in_cat'])){
      $sql="SELECT * FROM `xta_obj_category`";
      $reader =\Yii::$app->db->createCommand($sql)->query(); 
      foreach ($reader as $row){
          $value=0;
          if (isset($_POST['category'][$row['id']])&&intval($_POST['category'][$row['id']])){$value=1;}
          $count = fn__get_count_by_where('xta_obj_option_in_category',
                                          '`id_obj_option` = '.$id.' AND `id_obj_category`='.$row['id']);
          if ((!$value)&& $count){
             $sql=" DELETE FROM `xta_obj_option_in_category` 
                    WHERE `id_obj_option` =".$id." AND `id_obj_category` = ".$row['id'];
             \Yii::$app->db->createCommand($sql)->execute();
             }
          if (($value)&&(!$count)){
             $sql=" INSERT INTO `xta_obj_option_in_category`(`id_obj_option`, `id_obj_category`) 
                    VALUES (".$id.",".$row['id']."); ";
             \Yii::$app->db->createCommand($sql)->execute();
             }
        }
       $data['info']['active_tab']=2;
     }
   //----------------------------------------------------------------------------
   
   
   
   
   
   
    //echo $_POST['formdata'];
    
   // Добавляем\редактируем возможное значение
   //----------------------------------------------------------------------------
   if (isset($_POST['sbm_value'])){
       $_do = $_POST['formdata']['do'];
       $_value = sql_valid(strip_tags($_POST['formdata']['value']));
       $_id_value = intval($_POST['formdata']['id_value']);
       
       if ($_do=='edit'){
          $sql="UPDATE `xta_obj_option_available` SET 
                  `value`=".$_value."
                WHERE 
                  `id`=".$_id_value;
          }else{
          $sql="INSERT INTO `xta_obj_option_available`(
                  `id_obj_option`, 
                  `value`
                ) VALUES (
                  ".$id.",
                  ".$_value."
                )";
          }
        //echo $_do.'<br>'; print_r($_POST['formdata']); echo '<br>'.$sql;
        \Yii::$app->db->createCommand($sql)->execute(); 
        $data['info']['active_tab']=1;
     }
   //----------------------------------------------------------------------------
   
   
   
   
   
   
   
   //print_r($_POST); echo $command;
   
   // Удаляем возможное значение
   //---------------------------------------------------------------------------
   if (isset($_POST['delete_value'])&&(intval($_POST['delete_value']))){
      fn__del_record_by_id('xta_obj_option_available',intval($_POST['delete_value']));
      $data['info']['active_tab']=1;
      }
   //---------------------------------------------------------------------------

   

   $sql="SELECT * FROM `".$table."` WHERE `id` = ".$id;
   $row =\Yii::$app->db->createCommand($sql)->queryOne(); 
   $data['info']['name']=$row['name'];
   $data['info']['description']=$row['description'];
   $data['info']['isnumber']=$row['isnumber'];
   $data['info']['option_type']=$row['option_type'];
   $data['info']['category_tree']=fn__get_admin_category_tree($id);
   
   
   $tpl_val_item='
   <tr id="id_+id+_tr" class="class_tr_all">
    <td>+id+</td>
    <td>+value+</td>
    <td>
      <form method="post" id="select_value_id_+id+">
        <input type="hidden" name="delete_value" value="+id+">
      </form>
      <div class="btn-group btn-group-sm btn-group-select_item">
        <a title="Update" rel="tooltip" 
           class="btn btn-default" 
           href="javascript:void(0)" 
           role="button"
           onclick="$(\'#id_value\').val(\'+id+\'); 
                    $(\'#value_zn\').val(\'+value+\'); 
                    $(\'#do\').val(\'edit\'); 
                    $(\'#sbm_value\').val(\'Редактировать\'); 
                    $(\'.class_tr_all\').css(\'background-color\',\'#FFF\');  
                    $(\'#id_+id+_tr\').css(\'background-color\',\'#BEFF7E\');
                    $(\'html, body\').animate({ scrollTop: $(document).height()-$(window).height() });" 
           >
          <i class="glyphicon glyphicon-pencil"></i>
        </a>
        <a class="btn btn-default" 
           href="javascript:void(0)"
           role="button"
           onclick="$(\'form#select_value_id_+id+\').submit();">
          <span class="glyphicon glyphicon-trash"></span>
        </a>
      </div>
    </td>
   </tr>';

   $sql='SELECT * FROM `xta_obj_option_available` WHERE  `id_obj_option`='.$id.' ORDER BY `value` ASC ';
   $data['info']['values_items']=fn__get_select_by_sql_i_tpl($sql,$tpl_val_item);
   
}
//**************************************************************************************************












echo $this->render('view__'.$table,$data);
