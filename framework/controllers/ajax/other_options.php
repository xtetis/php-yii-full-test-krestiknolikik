<?
 use app\models\LibObj;
  use app\models\LibSql;

  $id = intval($id);
  
  // ПРоверяем корректновть ввода ID категории
  //============================================================================
  $correct_id = LibSql::fn__get_count_by_where('xta_obj_category','`id` = '.$id);
  if (!$correct_id){
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: /");
    exit();  
  }
  //============================================================================
  
  
  // ПРоверяем нет ли у категории дочерних категорий
  //============================================================================
  $correct_id = LibSql::fn__get_count_by_where('xta_obj_category','`id_parent` = '.$id);
  if ($correct_id){
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: /");
    exit();  
  }
  //============================================================================
  
  $ret = LibObj::fn__get_post_obj_options($id);
  echo $ret;
?>
