<?php


namespace app\controllers;


use Yii;
use yii\web\Controller;

include_once(__DIR__ . '/../libraries/tetislib.php');



class SiteController extends Controller {

  public $layout='main';
  public $enableCsrfValidation = false;
  

  public function init(){
    if (!fn__get_user_id()){
        header("Location: /login");
        exit();   
       }
  }




//******************************************************************************
	public function actionIndex(){	
   	include('site/index.php');
	}
//******************************************************************************







//******************************************************************************
	public function actionGame($id=Null){
  	$this->layout='empty';
	  include('site/game.php');
	}
//******************************************************************************






//******************************************************************************
	public function actionGetlastactiveasers(){ 
    echo fn__get_active_users();
	}
//******************************************************************************




//******************************************************************************
	public function actionActivity(){ 
	  $sql = "UPDATE `xta_user` SET `lastactivity` = NOW() WHERE `id`=".fn__get_user_id();
    \Yii::$app->db->createCommand($sql)->execute();
	}
//******************************************************************************





//******************************************************************************
	public function actionCreategame(){ 
	  $ret['status']=0;
	  $_gamer = intval($_POST['gamer']);
	  
	  $where='`id_user_gamer`='.fn__get_user_id().'
	          AND
         	  `id_user_creator`='.$_gamer.'  
	          AND 
	          `aborted`=0 AND `createdon`> NOW() - INTERVAL 2 MINUTE';
	  $createdgame_other = fn__get_count_by_where('xta_game',$where);

	  $where='`id_user_gamer`='.$_gamer.'
	          AND
         	  `id_user_creator`='.fn__get_user_id().'  
	          AND 
	          `aborted`=0 AND `createdon`> NOW() - INTERVAL 2 MINUTE';
	  $createdgame = fn__get_count_by_where('xta_game',$where);
	  
	  if ($createdgame){
       $ret['status']=1;
	  }elseif($createdgame_other){
	     $ret['status']=2;
	  }else{
	     
	     $sql = "INSERT INTO `xta_game`(`id_user_creator`, `id_user_gamer`) 
	            VALUES (".fn__get_user_id().",".$_gamer.")";
       \Yii::$app->db->createCommand($sql)->execute();
	  }
	  
    echo json_encode($ret);
	}
//******************************************************************************






//******************************************************************************
	public function actionAbortgame(){ 
	  $_game = intval($_POST['game']);
    $sql = "UPDATE `xta_game` SET `aborted`=1 WHERE `id` = ".$_game." AND `id_user_gamer` =".fn__get_user_id();
    \Yii::$app->db->createCommand($sql)->execute();
	}
//******************************************************************************




//******************************************************************************
	public function actionStartgame(){ 
	  $_game = intval($_POST['game']);
	  
	  $where='`id_user_gamer`='.fn__get_user_id().'
	          AND
         	  `id`='.$_game.'  
	          AND 
	          `aborted`=0';
	  $correct = fn__get_count_by_where('xta_game',$where);
	  if ($correct){
	  
	     }
	  
    $sql = "UPDATE `xta_game` SET `gamer_accept`=1 WHERE `id` = ".$_game;
    \Yii::$app->db->createCommand($sql)->execute();
	}
//******************************************************************************



//******************************************************************************
	public function actionSetstep(){ 
	  $_game = intval($_POST['game']);
	  $_step = intval($_POST['step']);
	  
	  if (!$this->actionCandostep($_game)){
    	  //return 'Это не Ваш ход';
    	  return 1;
	     }
	  
	  $where='`id_game`='.$_game.'
	          AND
         	  `position`='.$_step;
	  $count = fn__get_count_by_where('xta_step',$where);
	  if (!$count){
       $sql = "INSERT INTO `xta_step`(`id_game`, `id_user`, `position`)
               VALUES (".$_game.",".fn__get_user_id().",".$_step.")";
       \Yii::$app->db->createCommand($sql)->execute();

       
       $count_steps = fn__get_count_by_where('xta_step','`id_game`='.$_game);
       if ($count_steps==9){
          $this->actionClosegame($_game);
          return 3; // Шаги закончились
          }
       
       return 0;
   	   }else{
   	   //return 'Эта клетка уже занята';
   	   return 2;
   	   }
	  

	}
//******************************************************************************





//******************************************************************************
	public function actionCandostep($id=Null){ 
	  $_game = intval($id);
	  $id_user_creator = fn__get_field_val_by_id('xta_game','id_user_creator',$_game);
	  $id_user_gamer = fn__get_field_val_by_id('xta_game','id_user_gamer',$_game);
	  
	  
    $count_steps = fn__get_count_by_where('xta_step','`id_game`='.$_game);
    if ($count_steps==9){
       return 3; // Шаги закончились
       }
       
    $winner = $this->actionGetwinner($_game);
    if ($winner){
       //$this->Closegame($_game);
       return $winner; // Возвращаем победителя
       }
	  
	  $where='`id_game`='.$_game.'
	          AND
         	  `id_user`='.$id_user_creator;
	  $count_steps_creator = fn__get_count_by_where('xta_step',$where);
	  
	  $where='`id_game`='.$_game.'
	          AND
         	  `id_user`='.$id_user_gamer;
	  $count_steps_gamer = fn__get_count_by_where('xta_step',$where);
	  
	  if ($count_steps_creator<$count_steps_gamer){ // Черед создателя
	     if (fn__get_user_id()==$id_user_creator){
  	       return 1;
    	    }else{
           return 0;
    	    }
       }else{ // Черед второго игрока
	     if (fn__get_user_id()==$id_user_creator){
  	       return 0;
    	    }else{
           return 1;
    	    }
       }
	  
	  
	}
//******************************************************************************














//******************************************************************************
	public function actionAskgame($id=Null){ 
	  $id_user_creator = fn__get_field_val_by_id('xta_game','id_user_creator',$id);
	  $id_user_gamer = fn__get_field_val_by_id('xta_game','id_user_gamer',$id);
	  $id_user = $id_user_creator;
	  if ($id_user==fn__get_user_id()){
	     $id_user = $id_user_gamer;
	     }
	  $asktest = fn__lng_txt('Вы ходите начать новую игру?').'
	  <div class="btn-group" role="group" aria-label="...">
      <button type="button" class="btn btn-default" id="bth_try_again_yes"
              onclick="if ($(\'.createdgame_by_user_'.$id_user.'\').length) {
                           var idx = $(\'.createdgame_by_user_'.$id_user.'\').attr(\'idx\');
                           stop_all_intervals();
                           //alert(\'start_game = \'+idx);
                           start_game(idx);
                          }else{
                           create_game('.$id_user.'); window.location = \'/\';
                          }"
            >'.fn__lng_txt('Да').'</button>
      <button type="button" class="btn btn-default" data-dismiss="modal">'.fn__lng_txt('Нет').'</button>
    </div>';
    
    
	  echo $asktest;
	}
//******************************************************************************



//******************************************************************************
	public function actionGetacceptgame($id=Null){
	  $game = intval($id);
	  $aborted = fn__get_fieldval_by_id('xta_game','aborted',$game);
	  $gamer_accept = fn__get_fieldval_by_id('xta_game','gamer_accept',$game);
	  
	  if ($aborted){
	     return 2;
	     }
	  if ($gamer_accept){
	     return 1;
	     }
	  return 0;
	}
//******************************************************************************








// Победил ли кто в игре
//******************************************************************************
	public function actionGetwinner($id=Null){
	  $game = intval($id);
	  $id_user_creator = fn__get_field_val_by_id('xta_game','id_user_creator',$id);
	  $id_user_gamer = fn__get_field_val_by_id('xta_game','id_user_gamer',$id);
	  
	  $steps['creator'] = array();
	  $steps['gamer'] = array();
	  
	  $sql = "SELECT * FROM `xta_step` WHERE `id_game` = ".$game;
    $reader =\Yii::$app->db->createCommand($sql)->query(); 
    foreach ($reader as $row){
      if ($row['id_user']==$id_user_creator){
          $steps['creator'][]=$row['position'];
         }else{
          $steps['gamer'][]=$row['position'];
         }
    }
    
    //print_r($steps);
    $winarray = array(
                     array(1,2,3),
                     array(1,4,7),
                     array(1,5,9),
                     
                     array(2,5,8),
                     
                     array(3,6,9),
                     array(3,5,7),
                     
                     array(4,5,6),
                     
                     array(7,8,9),
                     );
    $ret = 0; // Никто не выиграл
    foreach ($winarray as $item){
      $diff = array_intersect($item, $steps['creator']);
      
      if (count($diff)==3){
         //print_r($diff); 
         $ret = 1; // Выиграл создатель игры
         }
         
      $diff = array_intersect($item, $steps['gamer']);
      if (count($diff)==3){
         //print_r($diff); 
         $ret = 2; // Выиграл приглашенный
         }
    }
    
    
    if ($ret){
        if ((fn__get_user_id()==$id_user_gamer)&&($ret==2)){
           return 11;
           }
        if ((fn__get_user_id()!=$id_user_gamer)&&($ret==2)){
           return 12;
           }
        if ((fn__get_user_id()==$id_user_gamer)&&($ret==1)){
           return 12;
           }
        if ((fn__get_user_id()!=$id_user_gamer)&&($ret==1)){
           return 11;
           }
       }
    return $ret;
	}
//******************************************************************************






//******************************************************************************
	public function actionClosegame($id=Null){
    $sql = "UPDATE `xta_game` SET `aborted`=1 WHERE `id` = ".$id;
    \Yii::$app->db->createCommand($sql)->execute();
	}
//******************************************************************************



}
