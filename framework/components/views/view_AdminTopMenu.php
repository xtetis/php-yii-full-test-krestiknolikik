    
    
    
<nav class="navbar navbar-default navbar-collapse" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/" target="frontend_window">
      <span class="glyphicon glyphicon-home"></span>
      <?=ucfirst($_SERVER['HTTP_HOST'])?></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="glyphicon glyphicon-info-sign"></span>
            Справочники <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/site/xta_book">Книги</a></li>
            <li class="divider"></li>
            <li><a href="/site/xta_setting_category">Категории настроек</a></li>
          </ul>
        </li>
        
        
      </ul>
 
 
 
 
 
 
 
 
 
 
 
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="glyphicon glyphicon-cog"></span>
          Настройки <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/site/xta_setting">Настройки сайта</a></li>
            <li class="disabled"><a href="#">Виджеты</a></li>
          </ul>
        </li>
        
        <li><a href="/login/logout" onclick="return confirm('Вы уверены, что хотите завершить сессию?');">
        <span class="glyphicon glyphicon-log-out"></span>
        Выход</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
    
    
    
    
    

    
    
    
    
  
