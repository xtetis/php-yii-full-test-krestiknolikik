<h4 class="text-center" style="margin-top: 0px;">
  <strong>
    <?=$params['title']?>
  </strong>
</h4>


<?//print_r($params);?>
<?if($params['show_all']||$params['show_new']||$params['show_view']):?>
<div class="btn-group btn-group-xs" style="padding-bottom:10px;">
  <?if($params['show_all']):?>
  <a class="btn btn-default" href="/site/<?=$params['function_name']?>">
     <span class="glyphicon glyphicon-th-list"></span> Список</a>
  <?endif;?>
  <?if($params['show_new']):?>
  <a class="btn btn-default" href="/site/<?=$params['function_name']?>/create">
     <span class="glyphicon glyphicon-plus"></span>
     Создать</a>
  <?endif;?>
  <?if($params['show_view']):?>
  <a class="btn btn-default" href="<?=$params['show_link']?>" target="frontend_window">
     <span class="glyphicon glyphicon-share-alt"></span>
     Просмотр</a>  
  <?endif;?>
</div>
<?endif;?>
