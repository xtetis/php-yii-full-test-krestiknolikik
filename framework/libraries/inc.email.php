<?php 



// Отправка сообщений пользователю
/* Плейсхолдеры
+http_host+
+date_now+
+username+
+link_main_autologin+
+link_myobj_autologin+
+link_mymess_autologin+
+link_settings_autologin+
*/
//****************************************************************************** 
function fn__send_email($to='',$subject='', $message='',$replace_placeholders=false,$id_user=0){ 
  $headers  = 'MIME-Version: 1.0' . "\r\n";
  $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
  $headers.= 'To: <'.$to.'>' . "\r\n";
  $headers.='From: '.ucfirst($_SERVER['HTTP_HOST']).' <noreply@'.$_SERVER['HTTP_HOST'].'>'."\r\n";
  
  if ($replace_placeholders){
     $date_now = date("Y-m-d H:i:s");
     $message = str_replace('+date_now+',$date_now,$message);
     
     $host = ucfirst($_SERVER['HTTP_HOST']);
     $message = str_replace('+http_host+',$host,$message);
     $subject = str_replace('+http_host+',$host,$subject);
     
     $uid = fn__get_user_id();
     if ($id_user){
        $uid = $id_user;
        }

     if ($uid){
          $message = str_replace('+link_main_autologin+',fn__get_link_autologin($uid),$message);
          
          $link = fn__get_link_autologin($uid,'account/obj');
          $message = str_replace('+link_myobj_autologin+',$link,$message);

          $link = fn__get_link_autologin($uid,'account/messages');
          $message = str_replace('+link_mymess_autologin+',$link,$message);
          
          $link = fn__get_link_autologin($uid,'account/settings');
          $message = str_replace('+link_settings_autologin+',$link,$message);
        }else{
          $link = 'http://'.$_SERVER['HTTP_HOST'].'/';
          $message = str_replace('+link_main_autologin+',$link,$message);
          
          $link = 'http://'.$_SERVER['HTTP_HOST'].'/account/obj';
          $message = str_replace('+link_myobj_autologin+',$link,$message);

          $link = 'http://'.$_SERVER['HTTP_HOST'].'/account/messages';
          $message = str_replace('+link_mymess_autologin+',$link,$message);
          
          $link = 'http://'.$_SERVER['HTTP_HOST'].'/account/settings';
          $message = str_replace('+link_settings_autologin+',$link,$message);
        }
        
        $username = fn__get_field_val_by_id('xta_user','username' ,$uid);
        $message = str_replace('+username+',$username,$message);
        
        $username = fn__get_field_val_by_id('xta_user','email' ,$uid);
        $message = str_replace('+email+',$username,$message);

     }
  
  mail($to, $subject, $message, $headers);
}
//******************************************************************************








//****************************************************************************** 
/*
function fn__send_email($to='',$subject='', $message=''){ 
  //date_default_timezone_set('Etc/UTC');
  require $_SERVER['DOCUMENT_ROOT'].'/protected/libraries/classes/phpmailer/PHPMailerAutoload.php';
  $mail = new PHPMailer;
  $mail->CharSet = "UTF-8";
  $mail->isSMTP();
//  $mail->SMTPDebug = 2;
//  $mail->Debugoutput = 'html';
  $mail->Host = 'smtp.gmail.com';
  $mail->Port = 587;
  $mail->SMTPSecure = 'tls';
  $mail->SMTPAuth = true;
  $mail->Username = base64_decode('dGlob25lbmtvdmFsZXJpeUBnbWFpbC5jb20'); //"tihonenkovaleriy@gmail.com";
  $mail->Password = base64_decode('Z29vZ2xlNzRqNGpqZGZr');// "google74j4jjdfk";
  $mail->setFrom('noreply@krgazeta.com', 'First Last');
  $mail->addAddress($to, $to);
  $mail->Subject = $subject;
  $mail->msgHTML($message, dirname(__FILE__));
  @$mail->send();

//  if (!$mail->send()) {
//      $ret= "Mailer Error: " . $mail->ErrorInfo;
//  } else {
//      $ret= "Message sent!";
//  }
//  file_put_contents($_SERVER['DOCUMENT_ROOT'].'/1.txt',$ret);

}
*/
//******************************************************************************









// Функция проверяет валидность email
//******************************************************************************
function fn__check_email($email){
  if(!preg_match("|^[-0-9a-z_\.]+@[-0-9a-z_^\.]+\.[a-z]{2,6}$|i", $email)){
    return FALSE;
    }else{
    return TRUE;
    }
}
//******************************************************************************











// Отправляем пользователю сообщение со ссылкой на изменение пароля
//******************************************************************************   
function fn__send_email_forgotpassword($email){

  // Отправляем письмо пользователю об отправке сообщения
  $tpl = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Восстановление пароля на сайте +http_host+</title>
</head>
<body>
<h3>Здравствуйте, +username+</h3>
<p class="lead">На сайте +http_host+ Вы воспользовались формой восстановления пароля:</p>
<table style="width:100%;">
  <tr>
    <td>Время отправления:</td>
    <td>+date_now+</td>
  </tr>
  <tr>
    <td>Для изменения пароля воспользуйтесь ccылкой:</td>
    <td><a href="+link_settings_autologin+">Изменить пароль</a></td>
  </tr>
</table>
<br>
<br>
<table style="width:100%;">
  <tr>
    <td><a href="+link_main_autologin+">+http_host+</a></td>
    <td><a href="+link_myobj_autologin+">Мои объявления</a></td>
    <td><a href="+link_mymess_autologin+">Мои сообщения</a></td>
  </tr>
</table>
</body>
</html>
  ';
  $sublect = 'Восстановление пароля на '.ucfirst($_SERVER['HTTP_HOST']);
  
  if (fn__get_count_by_where('xta_user','`email` = '.sql_valid($email))){
       $id_user = fn__get_fieldval_by_where('xta_user','id' ,
                          '`email` = '.sql_valid($email));
       fn__send_email($email,$sublect, $tpl,true,$id_user);
       return $id_user;
     }else{
       return 0;
     }
}
//******************************************************************************   







?>
