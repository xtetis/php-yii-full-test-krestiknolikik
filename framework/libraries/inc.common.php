<?php 



// Функция удаляет из строки все кроме цифр
//****************************************************************************** 
function fn__get_only_numbers($string=''){
  return preg_replace("/\D/","",$string);
}
//****************************************************************************** 







# Функция для генерации  md5 хеша случайной строки
//****************************************************************************** 
function fn__get_random_hash() {
  $length = 15;
  $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
  $code = "";
  $clen = strlen($chars) - 1;  
  while (strlen($code) < $length) {
    $code .= $chars[mt_rand(0,$clen)];  
  }
  return md5($code);
}
//****************************************************************************** 










# Функция для генерации  md5 хеша случайной строки
//****************************************************************************** 
function fn__code_by_key($do='encode',$txt='',$pass='') {
  function strcode($str, $passw="")
  {
     $salt = "Dn8*#2n!9j";
     $len = strlen($str);
     $gamma = '';
     $n = $len>100 ? 8 : 2;
     while( strlen($gamma)<$len )
     {
        $gamma .= substr(pack('H*', sha1($passw.$gamma.$salt)), 0, $n);
     }
     return $str^$gamma;
  }
  
  if ($do=='encode'){
    return base64_encode(strcode($txt,$pass));
  }else{
    return strcode(base64_decode($txt),$pass);
  }
}
//****************************************************************************** 





//******************************************************************************
function mb_ucfirst($str, $enc = 'utf-8') { 
		return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc).mb_substr($str, 1, mb_strlen($str, $enc), $enc); 
}
//******************************************************************************












# Функция проверяет валидность логина skype
//******************************************************************************
function fn__check_skype($skype)
{
if(!preg_match("/^[a-z][a-z0-9\.,\-_]{5,31}$/i", $skype))
    {
      return FALSE;
    }
    else{
          return TRUE;
        }
}
//******************************************************************************












# Функция для переадресации страницы с сообщением
//****************************************************************************** 
function fn__go_to_message_page($message=''){
  $pass = fn__get_random_hash();
  $encoded_text = fn__code_by_key('encode',$message,md5($_SERVER['REMOTE_ADDR'].$pass));
  setcookie("textpass", $pass, time()+3600, "/");
  
  $hash = fn__get_random_hash();
  $filename = dirname($_SERVER['DOCUMENT_ROOT']).'/temp/text/'.md5($_SERVER['REMOTE_ADDR'].$hash).'.bin';
  file_put_contents($filename,$encoded_text);
  
  header("Location: /text?hash=".$hash); 
  exit;
}
//****************************************************************************** 













// Функция русского отображения даты
//****************************************************************************** 
function rus_date() {
    $translate = array(
    "am" => "дп",
    "pm" => "пп",
    "AM" => "ДП",
    "PM" => "ПП",
    "Monday" => "Понедельник",
    "Mon" => "Пн",
    "Tuesday" => "Вторник",
    "Tue" => "Вт",
    "Wednesday" => "Среда",
    "Wed" => "Ср",
    "Thursday" => "Четверг",
    "Thu" => "Чт",
    "Friday" => "Пятница",
    "Fri" => "Пт",
    "Saturday" => "Суббота",
    "Sat" => "Сб",
    "Sunday" => "Воскресенье",
    "Sun" => "Вс",
    "January" => "Января",
    "Jan" => "Янв",
    "February" => "Февраля",
    "Feb" => "Фев",
    "March" => "Марта",
    "Mar" => "Мар",
    "April" => "Апреля",
    "Apr" => "Апр",
    "May" => "Мая",
    "May" => "Мая",
    "June" => "Июня",
    "Jun" => "Июн",
    "July" => "Июля",
    "Jul" => "Июл",
    "August" => "Августа",
    "Aug" => "Авг",
    "September" => "Сентября",
    "Sep" => "Сен",
    "October" => "Октября",
    "Oct" => "Окт",
    "November" => "Ноября",
    "Nov" => "Ноя",
    "December" => "Декабря",
    "Dec" => "Дек",
    "st" => "ое",
    "nd" => "ое",
    "rd" => "е",
    "th" => "ое"
    );
    
    if (func_num_args() > 1) {
        $timestamp = func_get_arg(1);
        return strtr(date(func_get_arg(0), $timestamp), $translate);
    } else {
        return strtr(date(func_get_arg(0)), $translate);
    }
}
//****************************************************************************** 












// Функция возвращает корректную страницу ()
// Входные параметры
// maxpage - Максимально возможная страница
// getparamname - Имя GET параметра для получения номера текущей страницы
//****************************************************************************** 
function fn__get_correct_page($maxpage=1, $getparamname='page'){ 
  $maxpage = intval($maxpage);
  if ($maxpage<1){$maxpage=1;}
  
  $page=(isset($_GET[$getparamname]))?intval($_GET[$getparamname]):0;
  if ($page>$maxpage){$page=1;}
  if ($page<1){$page=1;}

  return $page;
}
//******************************************************************************












// Функция возвращает страницы пагинации
// Входные параметры
// url - префикс урла
// totalpages - Всего страниц
// curpage - текущая страница
//****************************************************************************** 
function fn__get_pagination($url='/news?page=', $totalpages=1, $curpage=1){ 
  $curpage = intval($curpage);
  $totalpages = intval($totalpages);
  $ret='
       <div style="text-align:center; padding-left:50px; padding-right: 50px;">
          <div class="paginator"  id="paginator_example" style="width:100%;"></div>
       </div>
       <script type="text/javascript">
         function pagin() {
         paginator_example = new Paginator(
          "paginator_example", // id контейнера, куда ляжет пагинатор
           '.$totalpages.', // общее число страниц
           10, // число страниц, видимых одновременно
           '.$curpage.', // номер текущей страницы
           "'.$url.'" // url страниц
           );
            }          
         pagin();
       </script> 
  ';

  return $ret.fn__get_hidden_html_pagination($url, $totalpages, $curpage);
}
//******************************************************************************





// Функция возвращает страницы пагинации
// Входные параметры
// url - префикс урла
// totalpages - Всего страниц
// curpage - текущая страница
//****************************************************************************** 
function fn__get_hidden_html_pagination($url='/news?page=', $totalpages=1, $curpage=1){ 
  $curpage = intval($curpage);
  $totalpages = intval($totalpages);
  
  $ret='';
  for ($i = 1; $i <=$totalpages ; $i++)
  {
    if (($i<($curpage+5))&&($i>($curpage-5))){
       $ret.='<a href="'.$url.$i.'">'.$i.'</a>';
       }
  }
  $ret='
       <div class="hide">
          '.$ret.'
       </div>
  ';

  return $ret;
}
//******************************************************************************






















// Функция возвращает строку для перехода по фильтру
//****************************************************************************** 
function fn__get_params_to_url($staturl='/cat/1', $filter=array()){ 
  
  $fltr='';
  if (count($filter)){
  $fltr = http_build_query($filter);
  $fltr = str_replace('&',';',$fltr);
  $fltr = str_replace('=',':',$fltr);
  $fltr = '?filter='.$fltr;
  }
  $ret=$staturl.$fltr;
  
  
  return $ret;
}
//******************************************************************************













// Возвращает значение фильтра
//****************************************************************************** 
function fn__get_filters(){
  global $filters;
  $filter = $_GET['filter'];
  $filter = str_replace(';','&',$filter);
  $filter = str_replace(':','=',$filter);
  
  parse_str($filter, $filters);
//  return $output_filter[$name];
}
//****************************************************************************** 





// Возвращает строку фильтра
//****************************************************************************** 
function fn__get_filter_string(){
  global $filters;
  $ret = http_build_query($filters);
  $ret = str_replace('&',';',$ret);
  $ret = str_replace('=',':',$ret);
  return $ret;
}
//****************************************************************************** 















// Возвращает cгенерированный текст
//****************************************************************************** 
function fn__get_generated_text($text){
   static $result;
   if (preg_match("/^(.*)\{([^\{\}]+)\}(.*)$/isU", $text, $matches))
      {
      $p = explode('|', $matches[2]);
      foreach ($p as $comb)
         fn__get_generated_text($matches[1].$comb.$matches[3]);
      }
   else
      {
      $result[] = $text;
      return 0;
      }
   return array_values(array_unique($result));
}
//****************************************************************************** 









// Функция врзвращает текст с привязкой к ID
//****************************************************************************** 
function fn__get_generated_text_by_uid($t,$id){
  $num = fn__get_only_numbers(md5($id));
  $j=1;
	while ( preg_match( '#\{([^\{\}]+)\}#i', $t, $m ) ) {
		$v = explode( '|', $m[1] );
		$i = $num%count( $v );
		$num.=$num%$j;
		$t = preg_replace( '#'.preg_quote($m[0]).'#i', $v[$i], $t, 1 );
		$j++;
	}
 return $t;
}
//****************************************************************************** 







function GenTheText( $t ) {
	while ( preg_match( '#\{([^\{\}]+)\}#i', $t, $m ) ) {
		$v = explode( '|', $m[1] );
		$i = rand( 0, count( $v ) - 1 );
		$t = preg_replace( '#'.preg_quote($m[0]).'#i', $v[$i], $t, 1 );
	} return $t;
}








// Проверяет поле на корректность введенных данных
//$fields = array('field1'=>'value1', ... )
//$params = array(
//                array('required_fields',
//                      array('email'=>'Почта',
//                            'username'=>'Контактное лицо'
//                           )
//                     ),
//                array('length',
//                      array('email'=>'Почта'),
//                      array('minlength'=>6)
//                     ),
//                ...
//               )
//==============================================================================
function fn__validate_field_list($fields,$params){
  $res['errors']=array();
  //$res['error_fields'] = array();
  
  foreach ($params as $params_item){
     if ($params_item[0]=='required_fields'){
        foreach ($params_item[1] as $key=>$value){
          if ((!isset($fields[$key]))||(!strlen($fields[$key]))){
             $res['errors'][$key]=fn__lng_txt('Поле').' "'.$value.'" '.fn__lng_txt('не заполнено');
             }
          }
        }
        
     if ($params_item[0]=='length'){
        if (isset($params_item[2]['minlength'])){
           foreach ($params_item[1] as $key=>$value){
             if ((!isset($res['errors'][$key]))&&
                 (mb_strlen($fields[$key],'utf-8')<$params_item[2]['minlength'])){
                 $res['errors'][$key]=fn__lng_txt('Значение поля').' "'.$value.'" '.fn__lng_txt('меньше').' '.
                                      $params_item[2]['minlength'].' '.fn__lng_txt('символов').'';
                 }
             }
           }
        }
        
        
     if ($params_item[0]=='email'){
         foreach ($params_item[1] as $key=>$value){
           if ((!isset($res['errors'][$key]))&&(!fn__check_email($fields[$key]))){
               $res['errors'][$key]=fn__lng_txt('Значение поля').' "'.$value.'" '.fn__lng_txt('не является корректным адресом электронной почты');
               }
           }
        }
        
        
     if ($params_item[0]=='number'){
         foreach ($params_item[1] as $key=>$value){
           if ((!isset($res['errors'][$key]))&&
               (fn__get_only_numbers($fields[$key])<>$fields[$key])){
               $res['errors'][$key]=fn__lng_txt('В значении поля').' "'.$value.'" '.fn__lng_txt('должны присутствовать только цифры');
               }
           }
        }
        
     if ($params_item[0]=='equal'){
         $first_field = ''; $tmp_value = false; $counter=0;
         foreach ($params_item[1] as $key=>$value){
           if (!isset($res['errors'][$key])){
              if (!$counter){
                 $tmp_value = $fields[$key];
                 $first_field = $key;
                 }else{
                 if ($fields[$key]<>$tmp_value){
                    $flist = implode('" и "',$params_item[1]);
                    if (count($params_item[1])>2)$flist = implode('", "',$params_item[1]);
                    $res['errors'][$key]='Поля "'.$flist.'" '.fn__lng_txt('должны совпадать');
                    $res['errors'][$first_field]='Поля "'.$flist.'" '.fn__lng_txt('должны совпадать');
                    }
                 }
              $counter++;
              }
           }
        }
        
        
        
     if ($params_item[0]=='notexists'){
         foreach ($params_item[1] as $key=>$value){
           if ((!isset($res['errors'][$key]))&&
               (fn__get_count_by_where($params_item[2]['table'],
                                               '`'.$params_item[2]['field'].'`='.
                                               sql_valid($fields[$key])))){
               $res['errors'][$key]=$params_item[2]['message'];
               }
           }
        }
        
        
     }
  return $res;
}
//==============================================================================






?>
