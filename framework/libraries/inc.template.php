<?





// Функция возвращает меню пользователя, иначе ссылку для авторизации
//******************************************************************************   
function fn__get_user_menu(){   
 if (fn__get_user_id())
   {
     $ret='     
  <div class="btn-group">
   <a data-toggle="dropdown" class="dropdown-toggle btn-small" href="/account" style="color:#000; font-size:14px; padding:4px;">
  <img src="/themes/board/img/ico/user.png"> '.
    fn__get_field_val_by_id('xta_user','email',fn__get_user_id()).
    '<span class="caret"></span>
  </a>
  <ul id="usermenu_ul" class="dropdown-menu" style="width:200px;">
  <li id="usermenu_myobj"><img src="/themes/board/img/ico/list.png">'.fn__lng_txt('Управление').'</li>
  <li class="divider"></li>
  <li><a tabindex="-1" href="/login/logout">'.fn__lng_txt('Выйти').'</a></li>
  </ul>
  </div>   
     ';
   }else{
     $ret='
  
     ';
   }
 return $ret;  
}
//******************************************************************************   
   





// Функция возвращает меню пользователя, иначе ссылку для авторизации
//******************************************************************************   
function fn__get_lang_menu(){
  $lang = $_COOKIE['lang'];
  if (!in_array($lang,array('ru','en'))){
     $lang = 'ru';
     }
     
  $lang_full = 'Русский';
  if ($lang == 'en'){
     $lang_full = 'English';
  }
  
 $ret='
  <div class="dropdown">
    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
      '.$lang_full.'
      <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
      <li><a href="javascript:void(0)" 
             onclick="createCookie(\'lang\', \'ru\', 9); location.reload(); ">Русский</a></li>
      <li><a href="javascript:void(0)" 
             onclick="createCookie(\'lang\', \'en\', 9); location.reload(); ">English</a></li>
    </ul>
  </div>
 ';
 return $ret;  
}
//******************************************************************************   







// 
//******************************************************************************   
function fn__lng_txt($text_label){
  $lang = $_COOKIE['lang'];
  if (!in_array($lang,array('ru','en'))){
     $lang = 'ru';
     }
  
  include($_SERVER['DOCUMENT_ROOT'].'/framework/lang/'.$lang.'.php');
  
  if (isset($langarray[$text_label])){
     return $langarray[$text_label];
     }else{
     return $text_label;
     }
  
}
//******************************************************************************   





?>
