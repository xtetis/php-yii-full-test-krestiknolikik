<?
$langarray = array(

'Крестики-нолики'=>'Tic-tac-toe',

'Пароль'=>'Password',
'Войти'=>'Login',
'Регистрация'=>'Registration',
'Регистрация нового пользователя'=>'New User Registration',
'Минимальное количество символов - 7'=>'The minimum number of characters - 7',
'Контактное лицо'=>'The contact person',
'Минимальное количество символов - 4'=>'The minimum number of characters - 4',
'Укажите пароль не меньше 6 символов.'=>'Enter the password at least 6 characters.',
'Длина пароля не меньше 6 символов'=>'The password is at least 6 characters',
'Обязательное поле'=>'Required field',
'Подтверждение пароля'=>'Password confirmation',
'Для регистрации повторите введите пароль'=>'To register , enter the password again',
'Введите пароль повторно'=>'Enter password again',

'Укажите email. При регистрации Вы получите на этот email письмо с данными о регистрации.'=>'Enter the email. Upon registration you will receive an email message with the enrollment data',
'Укажите имя, которое будет указано в ваших объявлениях.'=>'Enter the name which will be shown in your ads',













'Успешно создана учетная запись'=>'Successfully created account',
'Логин'=>'Login',
'регистранионное описание'=>'<p> You can use this data to <a href="/login"> authorization online </a>. </p>
<p> The registration data sent to you by e-mail . </p>',




'Неверный email или пароль'=>'Invalid email or password',
'Такой email уже зарегистрирован'=>'This email is already registered',
'user_register_email_tpl'=>'
Welcome +username+. <br>
          You have successfully registered on the site http://+http_host+/<br> <br>
          To enter the site , use the following information : <br>
          Email: +email+ <br>
          Password : +pass+
          ',
'user_register_email_subject'=>'Thank you for registering on the site +http_host+',
'Имя пользователя'=>'Username',
'Последняя активность'=>'Last Activity',
'Предложить игру'=>'Suggest game',
'Игра предложена'=>'The game offered',
'Игра принята'=>'Game accepted',
'Предложена игра'=>'A game',
'Да'=>'Yes',
'Нет'=>'No',
'Список активных пользователей'=>'List of active users',
'Игра'=>'Game',
'Прекратить игру'=>'Stop the game',
'Ваш ход'=>'Your turn',
'Ходит ваш оппонент'=>'The turn of your opponent',
'Ячейка занята'=>'The cell is occupied',
'Шагов больше не осталось. Игра закончилась.'=>'Steps are no more . The game is over.',
'Вы победили'=>'You win',
'Вы проиграли'=>'You lost',
'Вы ходите начать новую игру?'=>'Do you go to start a new game ?',
'Поле'=>'Field',
'Значение поля'=>'The value of the field',
'В значении поля'=>'The value of the field',
'не заполнено'=>'is not filled',
'меньше'=>'less than',
'символов'=>'characters',
'не является корректным адресом электронной почты'=>' is not a valid email address',
'должны присутствовать только цифры'=>'must be only numbers',
'должны совпадать'=>'must match',



'Управление'=>'Management',
'Выйти'=>'Logout',


);

?>
